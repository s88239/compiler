#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "header.h"


int main( int argc, char *argv[] )
{
    FILE *source, *target;
    Program program;
    SymbolTable symtab;

    if( argc == 3){
        source = fopen(argv[1], "r");
        target = fopen(argv[2], "w");
        if( !source ){
            printf("can't open the source file\n");
            exit(2);
        }
        else if( !target ){
            printf("can't open the target file\n");
            exit(2);
        }
        else{
            //test_parser(source);
            program = parser(source);
            fclose(source);
            symtab = build(program);
            check(&program, &symtab);
            gencode(program, target, &symtab);
        }
    }
    else{
        printf("Usage: %s source_file target_file\n", argv[0]);
    }


    return 0;
}


/********************************************* 
  Scanning 
 *********************************************/
Token getNumericToken( FILE *source, char c )
{
    Token token;
    int i = 0;

    while( isdigit(c) ) {
        token.tok[i++] = c;
        c = fgetc(source);
    }

    if( c != '.' ){
        ungetc(c, source);
        token.tok[i] = '\0';
        token.type = IntValue;
        return token;
    }

    token.tok[i++] = '.';

    c = fgetc(source);
    if( !isdigit(c) ){
        ungetc(c, source);
        printf("Expect a digit : %c\n", c);
        exit(1);
    }

    while( isdigit(c) ){
        token.tok[i++] = c;
        c = fgetc(source);
    }

    ungetc(c, source);
    token.tok[i] = '\0';
    token.type = FloatValue;
    return token;
}

Token scanner( FILE *source )
{
    char c;
    Token token;

    while( !feof(source) ){
        c = fgetc(source);

        while( isspace(c) ) c = fgetc(source);

        if( isdigit(c) )
            return getNumericToken(source, c);

        token.tok[0] = c;
        token.tok[1] = '\0';
        token.length = 1;

        if ( islower(c) ){
            int length = 0;
            
            while( islower(c) ){
                token.tok[length++] = c;
                c = fgetc(source);  
            }
            ungetc(c, source);
            token.tok[length] = '\0';
            token.length = length;

            if ( strcmp(token.tok, "f") == 0 ){
                token.type = FloatDeclaration;
            }
            else if ( strcmp(token.tok, "i") == 0 ){
                token.type = IntegerDeclaration;
            }
            else if ( strcmp(token.tok, "p") == 0 ){
                token.type = PrintOp;
            }
            else{
                token.type = Alphabet;
            }
            return token;
        }

        /*if( islower(c) ){

            if( c == 'f' )
                token.type = FloatDeclaration;
            else if( c == 'i' )
                token.type = IntegerDeclaration;
            else if( c == 'p' )
                token.type = PrintOp;
            else
                token.type = Alphabet;
            return token;
        }*/
 
        switch(c){
            case '=':
                token.type = AssignmentOp;
                return token;
            case '+':
                token.type = PlusOp;
                return token;
            case '-':
                token.type = MinusOp;
                return token;
            case '*':
                token.type = MulOp;
                return token;
            case '/':
                token.type = DivOp;
                return token;
            case EOF:
                token.type = EOFsymbol;
                token.length = 0;
                token.tok[0] = '\0';
                return token;
            default:
                printf("Invalid character : %c\n", c);
                exit(1);
        }
    }

    token.tok[0] = '\0';
    token.length = 0;
    token.type = EOFsymbol;
    return token;
}


/********************************************************
  Parsing
 *********************************************************/
Declaration parseDeclaration( FILE *source, Token token )
{
    Token token2;
    switch(token.type){
        case FloatDeclaration:
        case IntegerDeclaration:
            token2 = scanner(source);
            //printf("%s %s\n", token.tok, token2.tok);
            if (strcmp(token2.tok, "f") == 0 ||
                    strcmp(token2.tok, "i") == 0 ||
                    strcmp(token2.tok, "p") == 0) {
                printf("Syntax Error: %s cannot be used as id\n", token2.tok);
                exit(1);
            }
            return makeDeclarationNode( token, token2 );
        default:
            printf("Syntax Error: Expect Declaration %s\n", token.tok);
            exit(1);
    }
}

Declarations *parseDeclarations( FILE *source )
{
    Token token = scanner(source);
    Declaration decl;
    Declarations *decls;
    int i;
    switch(token.type){
        case FloatDeclaration:
        case IntegerDeclaration:
            decl = parseDeclaration(source, token);
            decls = parseDeclarations(source);
            return makeDeclarationTree( decl, decls );
        case Alphabet:
            for ( i = token.length-1; i >= 0; i-- ){
                ungetc(token.tok[i], source);
            }
            return NULL;
        case PrintOp:
            ungetc(token.tok[0], source);
            return NULL;
        case EOFsymbol:
            return NULL;
        default:
            printf("Syntax Error: Expect declarations %s\n", token.tok);
            exit(1);
    }
}

Expression *parseRealvalue( Token token )
{
    Expression *value = (Expression *)malloc( sizeof(Expression) );
    value->leftOperand = value->rightOperand = NULL;

    Expression *substree = (Expression *)malloc( sizeof(Expression) );
    (substree->v).type = MinusNode;
    (substree->v).val.op = Minus;
    Token token2;
    token2.type = IntValue;
    strcpy(token2.tok, "0");
    token2.length = 1;

    switch(token.type){
        case Alphabet:
            (value->v).type = Identifier;
            (value->v).val.id = malloc(token.length+1);
            strcpy((value->v).val.id, token.tok);
            //(value->v).val.id = token.tok[0];
            break;
        case IntValue:
            (value->v).type = IntConst;
            (value->v).val.ivalue = atoi(token.tok);
            break;
        case FloatValue:
            (value->v).type = FloatConst;
            (value->v).val.fvalue = atof(token.tok);
            break;
        default:
            printf("Syntax Error: Expect Identifier or a Number %s\n", token.tok);
            exit(1);
    }

    return value;
}

Expression *parseValue( FILE *source ){
    Token token = scanner(source);
    Expression *value = (Expression *)malloc( sizeof(Expression) );
    value->leftOperand = value->rightOperand = NULL;

    switch(token.type){
        case MinusOp: // Negtive Number
            // get the value of next token
            token = scanner(source);
            value = parseRealvalue(token);

            // transform to negtive value
            Expression *substree = (Expression *)malloc( sizeof(Expression) );
            (substree->v).type = MinusNode;
            (substree->v).val.op = Minus;
            Token token2;
            token2.type = IntValue;
            strcpy(token2.tok, "0");
            token2.length = 1;

            /*substree->leftOperand = parseRealvalue(token2);
            substree->rightOperand = value;

            return substree;*/

            if( (value->v).type==IntConst ){
                (value->v).val.ivalue *= -1;
            }
            else if( (value->v).type==FloatConst ){
                (value->v).val.fvalue *= -1;
            }
            else if ( (value->v).type == Identifier ){
                substree->leftOperand = parseRealvalue(token2);
                substree->rightOperand = value;

                return substree;
            }
            break;
        default:
            value = parseRealvalue(token);
            break;
    }

    return value;
}

Expression *parseConstantFolding( Expression *expr ){
    Expression *lvalue = expr->leftOperand;
    Expression *rvalue = expr->rightOperand;
    // both subtrees must be numerics
    if( (lvalue->v).type!=IntConst && (lvalue->v).type!=FloatConst
    || (rvalue->v).type!=IntConst && (rvalue->v).type!=FloatConst )
        return expr;

    // do constant folding
    Expression *new_expr = (Expression *)malloc( sizeof(Expression) );
    new_expr->leftOperand = new_expr->rightOperand = NULL;

    /*if ( (lvalue->v).type == MinusNode && (lvalue->leftOperand->v).val.ivalue == 0 &&
         (lvalue->rightOperand->v).type == IntConst ){
        printf("OKOKOK1\n");
        printf("%i\n", (lvalue->rightOperand->v).val.ivalue);
        printf("%i\n", (rvalue->rightOperand->v).val.ivalue);
        (lvalue->v).type = IntConst;
        (lvalue->v).val.ivalue = 0 - (lvalue->rightOperand->v).val.ivalue;
        lvalue->leftOperand = lvalue->rightOperand = NULL;
    }
    else if ( (lvalue->v).type == MinusNode && (lvalue->leftOperand->v).val.ivalue == 0 &&
              (lvalue->rightOperand->v).type == FloatConst ){
        printf("OKOKOK2\n");
        (lvalue->v).type = FloatConst;
        (lvalue->v).val.fvalue = 0.0 - (lvalue->rightOperand->v).val.fvalue;
        lvalue->leftOperand = lvalue->rightOperand = NULL;
    }
    else if ( (rvalue->v).type == MinusNode && (rvalue->leftOperand->v).val.ivalue == 0 &&
         (rvalue->rightOperand->v).type == IntConst ){
        printf("OKOKOK3\n");
        //printf("%i\n", (rvalue->rightOperand->v).val.ivalue);
        //printf("%i\n", (lvalue->v).type);
        (rvalue->v).type = IntConst;
        (rvalue->v).val.ivalue = 0 - (rvalue->rightOperand->v).val.ivalue;
        rvalue->leftOperand = rvalue->rightOperand = NULL;
    }
    else if ( (rvalue->v).type == MinusNode && (rvalue->leftOperand->v).val.ivalue == 0 &&
              (rvalue->rightOperand->v).type == FloatConst ){
        printf("OKOKOK4\n");
        printf("%f\n", (rvalue->rightOperand->v).val.fvalue);
        (rvalue->v).type = FloatConst;
        (rvalue->v).val.fvalue = 0.0 - (rvalue->rightOperand->v).val.fvalue;
        rvalue->leftOperand = rvalue->rightOperand = NULL;
    }*/

    if( (lvalue->v).type==FloatConst && (rvalue->v).type==FloatConst ){
        (new_expr->v).type = FloatConst;
        switch( (expr->v).val.op ){
            case Plus:
                (new_expr->v).val.fvalue = (lvalue->v).val.fvalue + (rvalue->v).val.fvalue;
                break;
            case Minus:
                (new_expr->v).val.fvalue = (lvalue->v).val.fvalue - (rvalue->v).val.fvalue;
                break;
            case Mul:
                (new_expr->v).val.fvalue = (lvalue->v).val.fvalue * (rvalue->v).val.fvalue;
                break;
            case Div:
                (new_expr->v).val.fvalue = (lvalue->v).val.fvalue / (rvalue->v).val.fvalue;
                break;
            default:
                printf("(f,f) Must be +-*/\n");
        }
        return new_expr;
    }
    else if( (lvalue->v).type==FloatConst && (rvalue->v).type==IntConst ){
        (new_expr->v).type = FloatConst;
        switch( (expr->v).val.op ){
            case Plus:
                (new_expr->v).val.fvalue = (lvalue->v).val.fvalue + (rvalue->v).val.ivalue;
                break;
            case Minus:
                (new_expr->v).val.fvalue = (lvalue->v).val.fvalue - (rvalue->v).val.ivalue;
                break;
            case Mul:
                (new_expr->v).val.fvalue = (lvalue->v).val.fvalue * (rvalue->v).val.ivalue;
                break;
            case Div:
                (new_expr->v).val.fvalue = (lvalue->v).val.fvalue / (rvalue->v).val.ivalue;
                break;
            default:
                printf("(f,i) Must be +-*/\n");
        }
        return new_expr;
    }
    else if( (lvalue->v).type==IntConst && (rvalue->v).type==FloatConst ){
        (new_expr->v).type = FloatConst;
        switch( (expr->v).val.op ){
            case Plus:
                (new_expr->v).val.fvalue = (lvalue->v).val.ivalue + (rvalue->v).val.fvalue;
                break;
            case Minus:
                (new_expr->v).val.fvalue = (lvalue->v).val.ivalue - (rvalue->v).val.fvalue;
                break;
            case Mul:
                (new_expr->v).val.fvalue = (lvalue->v).val.ivalue * (rvalue->v).val.fvalue;
                break;
            case Div:
                (new_expr->v).val.fvalue = (lvalue->v).val.ivalue / (rvalue->v).val.fvalue;
                break;
            default:
                printf("(i,f) Must be +-*/\n");
        }
        return new_expr;
    }
    else if( (lvalue->v).type==IntConst && (rvalue->v).type==IntConst ){
        (new_expr->v).type = IntConst;
        switch( (expr->v).val.op ){
            case Plus:
                (new_expr->v).val.ivalue = (lvalue->v).val.ivalue + (rvalue->v).val.ivalue;
                break;
            case Minus:
                (new_expr->v).val.ivalue = (lvalue->v).val.ivalue - (rvalue->v).val.ivalue;
                break;
            case Mul:
                (new_expr->v).val.ivalue = (lvalue->v).val.ivalue * (rvalue->v).val.ivalue;
                break;
            case Div:
                (new_expr->v).val.ivalue = (lvalue->v).val.ivalue / (rvalue->v).val.ivalue;
                break;
            default:
                printf("(i,i) Must be +-*/\n");
        }
        return new_expr;
    }

    //handle negative number
    /*Expression *substree = (Expression *)malloc( sizeof(Expression) );
    Expression *value = (Expression *)malloc( sizeof(Expression) );
    value->leftOperand = value->rightOperand = NULL;
    (substree->v).type = MinusNode;
    (substree->v).val.op = Minus;
    Token token2;
    token2.type = IntValue;
    strcpy(token2.tok, "0");
    token2.length = 1;
    substree->leftOperand = parseRealvalue(token2);
    if ( (lvalue->v).type==IntConst && (rvalue->v).type!=IntConst && (rvalue->v).type!=FloatConst ){
        if ( (lvalue->v).val.ivalue < 0 ){
            (value->v).type = IntConst;
            (value->v).val.ivalue = 0 - (lvalue->v).val.ivalue;
            substree->rightOperand = value;
            expr->leftOperand = substree;
        }
        return expr;
    }
    else if ( (lvalue->v).type==FloatConst && (rvalue->v).type!=IntConst && (rvalue->v).type!=FloatConst ){
        if ( (lvalue->v).val.fvalue < 0 ){
            (value->v).type = FloatConst;
            (value->v).val.fvalue = 0 - (lvalue->v).val.fvalue;
            substree->rightOperand = value;
            expr->leftOperand = substree;
        }
        return expr;
    }
    else if ( (rvalue->v).type==IntConst && (lvalue->v).type!=IntConst && (lvalue->v).type!=FloatConst ){
        if ( (rvalue->v).val.ivalue < 0 ){
            (value->v).type = IntConst;
            (value->v).val.ivalue = 0 - (rvalue->v).val.ivalue;
            substree->rightOperand = value;
            expr->rightOperand = substree;
        }
        return expr;
    }
    else if ( (rvalue->v).type==FloatConst && (lvalue->v).type!=IntConst && (lvalue->v).type!=FloatConst ){
        if ( (rvalue->v).val.fvalue < 0 ){
            (value->v).type = FloatConst;
            (value->v).val.fvalue = 0 - (rvalue->v).val.fvalue;
            substree->rightOperand = value;
            expr->rightOperand = substree;
        }
        return expr;
    }
    else
        return expr;*/

    
}

Expression *parseTerm( FILE *source, Expression *lvalue )
{
    Token token = scanner(source);
    Expression *term = (Expression *)malloc( sizeof(Expression) );
    //term->leftOperand = term->rightOperand = NULL;

    //printf("%s\n", token.tok);

    switch(token.type){
        int i;
        case MulOp:
            (term->v).type = MulNode;
            (term->v).val.op = Mul;
            term->leftOperand = lvalue;
            term->rightOperand = parseValue(source);
            term = parseConstantFolding(term);
            return parseTerm(source, term);
        case DivOp:
            (term->v).type = DivNode;
            (term->v).val.op = Div;
            term->leftOperand = lvalue;
            term->rightOperand = parseValue(source);
            term = parseConstantFolding(term);
            return parseTerm(source, term);
        case Alphabet:
            for ( i = token.length-1; i >= 0; i-- ){
                ungetc(token.tok[i], source);
            }
            return lvalue;
        case PrintOp:
            ungetc(token.tok[0], source);
            return lvalue;
        case EOFsymbol:
            return lvalue;
        default:
            ungetc(token.tok[0], source);
            return lvalue;
    }
    
}

Expression *parseExpressionTail( FILE *source, Expression *lvalue )
{
    Token token = scanner(source);
    Expression *expr;
    //expr->leftOperand = expr->rightOperand = NULL;

    switch(token.type){
        int i;
        case PlusOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = PlusNode;
            (expr->v).val.op = Plus;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseTerm(source, parseValue(source));
            //expr->rightOperand = parseValue(source);
            expr = parseConstantFolding(expr);
            return parseExpressionTail(source, expr);
        case MinusOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = MinusNode;
            (expr->v).val.op = Minus;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseTerm(source, parseValue(source));
            //expr->rightOperand = parseValue(source);
            expr = parseConstantFolding(expr);
            return parseExpressionTail(source, expr);
        case Alphabet:
            for ( i = token.length-1; i >= 0; i-- ){
                ungetc(token.tok[i], source);
            }
            return lvalue;
        case PrintOp:
            ungetc(token.tok[0], source);
            return lvalue;
        case EOFsymbol:
            return lvalue;
        default:
            printf("Syntax Error: Expect a numeric value or an identifier %s\n", token.tok);
            exit(1);
    }
}

Expression *parseExpression( FILE *source, Expression *lvalue )
{
    Token token = scanner(source);
    Expression *expr;
    //expr->leftOperand = expr->rightOperand = NULL;
    switch(token.type){
        int i;
        case PlusOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = PlusNode;
            (expr->v).val.op = Plus;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseTerm(source, parseValue(source));
            //expr->rightOperand = parseValue(source);
            expr = parseConstantFolding(expr);
            return parseExpressionTail(source, expr);
        case MinusOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = MinusNode;
            (expr->v).val.op = Minus;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseTerm(source, parseValue(source));
            //expr->rightOperand = parseValue(source);
            expr = parseConstantFolding(expr);
            return parseExpressionTail(source, expr);
        case Alphabet:
            for ( i = token.length-1; i >= 0; i-- ){
                ungetc(token.tok[i], source);
            }
            return NULL;
        case PrintOp:
            ungetc(token.tok[0], source);
            return NULL;
        case EOFsymbol:
            return NULL;
        default:
            printf("Syntax Error: Expect a numeric value or an identifier %s\n", token.tok);
            exit(1);
    }
}

Expression *handleNegativeNumber( Expression *expr ){
    if(expr->leftOperand == NULL && expr->rightOperand == NULL){
        Expression *substree = (Expression *)malloc( sizeof(Expression) );
        (substree->v).type = MinusNode;
        (substree->v).val.op = Minus;
        Token token2;
        token2.type = IntValue;
        strcpy(token2.tok, "0");
        token2.length = 1;

        Expression *value = (Expression *)malloc( sizeof(Expression) );
        value->leftOperand = value->rightOperand = NULL;
        if ( (expr->v).type == IntConst && (expr->v).val.ivalue < 0 ){
            int tmp = (expr->v).val.ivalue;
            substree->leftOperand = parseRealvalue(token2);
            (value->v).type = IntConst;
            (value->v).val.ivalue = 0 - tmp;
            substree->rightOperand = value;
            expr = substree;
            return substree;
        }
        else if ( (expr->v).type == FloatConst && (expr->v).val.fvalue < 0.0 ){
            float tmp = (expr->v).val.fvalue;
            substree->leftOperand = parseRealvalue(token2);
            (value->v).type = FloatConst;
            (value->v).val.fvalue = 0.0 - tmp;
            substree->rightOperand = value;
            expr = substree;
            return substree;
        }
        return expr;
    }
    else{
        Expression *left = expr->leftOperand;
        Expression *right = expr->rightOperand;

        expr->leftOperand = handleNegativeNumber(expr->leftOperand);
        expr->rightOperand = handleNegativeNumber(expr->rightOperand);
        return expr;
    }
}

Statement parseStatement( FILE *source, Token token )
{
    Token next_token;
    Expression *value, *expr, *term;

    switch(token.type){
        case Alphabet:
            next_token = scanner(source);
            //printf("%s %s\n", token.tok, next_token.tok);
            if(next_token.type == AssignmentOp){
                //value = parseValue(source);
                //expr = parseExpression(source, value);
                //return makeAssignmentNode(token.tok[0], value, expr);
                term = parseTerm(source, parseValue(source));
                expr = parseExpression(source, term);
                //return makeAssignmentNode(token.tok[0], term, expr);

                //handle negative number
                if ( expr == NULL )
                    term = handleNegativeNumber(term);
                else
                    expr = handleNegativeNumber(expr);

                return makeAssignmentNode(token.tok, term, expr);
            }
            else{
                printf("Syntax Error: Expect an assignment op %s\n", next_token.tok);
                exit(1);
            }
        case PrintOp:
            next_token = scanner(source);
            if(next_token.type == Alphabet)
                return makePrintNode(next_token.tok);
                //return makePrintNode(next_token.tok[0]);
            else{
                printf("Syntax Error: Expect an identifier %s\n", next_token.tok);
                exit(1);
            }
            break;
        default:
            printf("Syntax Error: Expect a statement %s\n", token.tok);
            exit(1);
    }
}

Statements *parseStatements( FILE * source )
{

    Token token = scanner(source);
    Statement stmt;
    Statements *stmts;
    switch(token.type){
        case Alphabet:
        case PrintOp:
            stmt = parseStatement(source, token);
            stmts = parseStatements(source);
            return makeStatementTree(stmt , stmts);
        case EOFsymbol:
            return NULL;
        default:
            printf("Syntax Error: Expect statements %s\n", token.tok);
            exit(1);
    }
}


/*********************************************************************
  Build AST
 **********************************************************************/
Declaration makeDeclarationNode( Token declare_type, Token identifier )
{
    Declaration tree_node;
    switch(declare_type.type){
        case FloatDeclaration:
            tree_node.type = Float;
            break;
        case IntegerDeclaration:
            tree_node.type = Int;
            break;
        default:
            break;
    }
    //tree_node.name = identifier.tok[0];
    tree_node.name = malloc(identifier.length+1);
    strcpy(tree_node.name, identifier.tok);

    return tree_node;
}

Declarations *makeDeclarationTree( Declaration decl, Declarations *decls )
{
    Declarations *new_tree = (Declarations *)malloc( sizeof(Declarations) );
    new_tree->first = decl;
    new_tree->rest = decls;

    return new_tree;
}


Statement makeAssignmentNode( char *id, Expression *v, Expression *expr_tail )
{
    Statement stmt;
    AssignmentStatement assign;
    stmt.type = Assignment;
    //assign.id = id;
    assign.id = malloc(strlen(id)+1);
    strcpy(assign.id, id);
    if(expr_tail == NULL)
        assign.expr = v;
    else
        assign.expr = expr_tail;
    stmt.stmt.assign = assign;

    return stmt;
}

Statement makePrintNode( char *id )
{
    Statement stmt;
    stmt.type = Print;
    //stmt.stmt.variable = id;
    stmt.stmt.variable = malloc(strlen(id)+1);
    strcpy(stmt.stmt.variable, id);

    return stmt;
}

Statements *makeStatementTree( Statement stmt, Statements *stmts )
{
    Statements *new_tree = (Statements *)malloc( sizeof(Statements) );
    new_tree->first = stmt;
    new_tree->rest = stmts;

    return new_tree;
}

/* parser */
Program parser( FILE *source )
{
    Program program;

    program.declarations = parseDeclarations(source);
    program.statements = parseStatements(source);

    return program;
}


/********************************************************
  Build symbol table
 *********************************************************/
void InitializeTable( SymbolTable *table )
{
    int i;

    for(i = 0 ; i < 26; i++){
        table->table[i] = Notype;
    }
}

void add_table( SymbolTable *table, char *c, DataType t, int index )
{
    //int index = (int)(c - 'a');

    if(table->table[index] != Notype)
        printf("Error : id %s has been declared\n", c);//error
    table->table[index] = t;
    table->id[index] = malloc(strlen(c)+1);
    strcpy(table->id[index], c);
}

SymbolTable build( Program program )
{
    SymbolTable table;
    Declarations *decls = program.declarations;
    Declaration current;

    InitializeTable(&table);

    int index = 0;
    while(decls !=NULL){
        current = decls->first;
        //printf("%s\n", current.name);
        add_table(&table, current.name, current.type, index);
        decls = decls->rest;
        index++;
    }

    table.total_index = index;
    return table;
}


/********************************************************************
  Type checking
 *********************************************************************/

void convertType( Expression * old, DataType type )
{
    if(old->type == Float && type == Int){
        printf("error : can't convert float to integer\n");
        return;
    }
    if(old->type == Int && type == Float){
        Expression *tmp = (Expression *)malloc( sizeof(Expression) );
        if(old->v.type == Identifier)
            printf("convert to float %s \n",old->v.val.id);
        else
            printf("convert to float %d \n", old->v.val.ivalue);
        tmp->v = old->v;
        tmp->leftOperand = old->leftOperand;
        tmp->rightOperand = old->rightOperand;
        tmp->type = old->type;

        Value v;
        v.type = IntToFloatConvertNode;
        v.val.op = IntToFloatConvert;
        old->v = v;
        old->type = Int;
        old->leftOperand = tmp;
        old->rightOperand = NULL;
    }
}

DataType generalize( Expression *left, Expression *right )
{
    if(left->type == Float || right->type == Float){
        printf("generalize : float\n");
        return Float;
    }
    printf("generalize : int\n");
    return Int;
}

DataType lookup_table( SymbolTable *table, char *c)
{
    int i, id = -1;
    for ( i = 0; i < table->total_index; i++ ){
        if( strcmp(table->id[i] ,c) == 0 ){
            id = i;
            break;
        }
    }
    if( id==-1 || table->table[id] != Int && table->table[id] != Float){
        printf("Error : identifier %s is not declared\n", c);//error
        return Notype;
    }
    return table->table[id];
}

void checkexpression( Expression * expr, SymbolTable * table )
{
    char c[257];

    if(expr->leftOperand == NULL && expr->rightOperand == NULL){
        switch(expr->v.type){
            case Identifier:
                //c = expr->v.val.id;
                //c = malloc(strlen(expr->v.val.id));
                strcpy(c, expr->v.val.id);
                printf("identifier : %s\n",c);
                expr->type = lookup_table(table, c);
                break;
            case IntConst:
                printf("constant : int\n");
                expr->type = Int;
                break;
            case FloatConst:
                printf("constant : float\n");
                expr->type = Float;
                break;
                //case PlusNode: case MinusNode: case MulNode: case DivNode:
            default:
                break;
        }
    }
    else{
        Expression *left = expr->leftOperand;
        Expression *right = expr->rightOperand;

        checkexpression(left, table);
        checkexpression(right, table);

        DataType type = generalize(left, right);
        convertType(left, type);//left->type = type;//converto
        convertType(right, type);//right->type = type;//converto
        expr->type = type;
    }
}

void checkstmt( Statement *stmt, SymbolTable * table )
{
    if(stmt->type == Assignment){
        AssignmentStatement assign = stmt->stmt.assign;
        printf("assignment : %s \n",assign.id);
        checkexpression(assign.expr, table);
        stmt->stmt.assign.type = lookup_table(table, assign.id);
        if (assign.expr->type == Float && stmt->stmt.assign.type == Int) {
            printf("error : can't convert float to integer\n");
        } else {
            convertType(assign.expr, stmt->stmt.assign.type);
        }
    }
    else if (stmt->type == Print){
        printf("print : %s \n",stmt->stmt.variable);
        lookup_table(table, stmt->stmt.variable);
    }
    else printf("error : statement error\n");//error
}

void check( Program *program, SymbolTable * table )
{
    Statements *stmts = program->statements;
    while(stmts != NULL){
        checkstmt(&stmts->first,table);
        stmts = stmts->rest;
    }
}


/***********************************************************************
  Code generation
 ************************************************************************/
void fprint_op( FILE *target, ValueType op )
{
    switch(op){
        case MinusNode:
            fprintf(target,"-\n");
            break;
        case PlusNode:
            fprintf(target,"+\n");
            break;
        case MulNode:
            fprintf(target,"*\n");
            break;
        case DivNode:
            fprintf(target,"/\n");
            break;
        default:
            fprintf(target,"Error in fprintf_op ValueType = %d\n",op);
            break;
    }
}

void fprint_expr( FILE *target, Expression *expr, SymbolTable * table)
{
    int i, index; 
    char c;
    if(expr->leftOperand == NULL){
        switch( (expr->v).type ){
            case Identifier:
                for ( i = 0; i < table->total_index; i++ ){
                    if ( strcmp((expr->v).val.id, table->id[i]) == 0 ){
                        index = i;
                        break;
                    }
                }
                c = 'a' + index;
                if ( c == 'i' ){
                    c = 'x';
                }
                else if ( c == 'f' ){
                    c = 'y';
                }
                else if ( c == 'p' ){
                    c = 'z';
                }
                fprintf(target,"l%c\n",c);
                break;
            case IntConst:
                fprintf(target,"%d\n",(expr->v).val.ivalue);
                break;
            case FloatConst:
                fprintf(target,"%f\n", (expr->v).val.fvalue);
                break;
            default:
                fprintf(target,"Error In fprint_left_expr. (expr->v).type=%d\n",(expr->v).type);
                break;
        }
    }
    else{
        fprint_expr(target, expr->leftOperand, table);
        if(expr->rightOperand == NULL){
            fprintf(target,"5k\n");
        }
        else{
            //	fprint_right_expr(expr->rightOperand);
            fprint_expr(target, expr->rightOperand, table);
            fprint_op(target, (expr->v).type);
        }
    }
}

void gencode(Program prog, FILE * target, SymbolTable * table)
{
    Statements *stmts = prog.statements;
    Statement stmt;

    while(stmts != NULL){
        stmt = stmts->first;
        int i, index; 
        char c;
        switch(stmt.type){
            case Print:
                for ( i = 0; i < table->total_index; i++ ){
                    if ( strcmp(stmt.stmt.variable, table->id[i]) == 0 ){
                        index = i;
                        break;
                    }
                }
                c = 'a' + index;
                if ( c == 'i' ){
                    c = 'x';
                }
                else if ( c == 'f' ){
                    c = 'y';
                }
                else if ( c == 'p' ){
                    c = 'z';
                }
                fprintf(target,"l%c\n", c);
                fprintf(target,"p\n");
                break;
            case Assignment:
                fprint_expr(target, stmt.stmt.assign.expr, table);
                /*
                   if(stmt.stmt.assign.type == Int){
                   fprintf(target,"0 k\n");
                   }
                   else if(stmt.stmt.assign.type == Float){
                   fprintf(target,"5 k\n");
                   }*/
                for ( i = 0; i < table->total_index; i++ ){
                    if ( strcmp(stmt.stmt.assign.id, table->id[i]) == 0 ){
                        index = i;
                        break;
                    }
                }
                c = 'a' + index;
                if ( c == 'i' ){
                    c = 'x';
                }
                else if ( c == 'f' ){
                    c = 'y';
                }
                else if ( c == 'p' ){
                    c = 'z';
                }

                fprintf(target,"s%c\n", c);
                fprintf(target,"0 k\n");
                break;
        }
        stmts=stmts->rest;
    }

}


/***************************************
  For our debug,
  you can omit them.
 ****************************************/
void print_expr(Expression *expr)
{
    if(expr == NULL)
        return;
    else{
        print_expr(expr->leftOperand);
        switch((expr->v).type){
            case Identifier:
                printf("%s ", (expr->v).val.id);
                break;
            case IntConst:
                printf("%d ", (expr->v).val.ivalue);
                break;
            case FloatConst:
                printf("%f ", (expr->v).val.fvalue);
                break;
            case PlusNode:
                printf("+ ");
                break;
            case MinusNode:
                printf("- ");
                break;
            case MulNode:
                printf("* ");
                break;
            case DivNode:
                printf("/ ");
                break;
            case IntToFloatConvertNode:
                printf("(float) ");
                break;
            default:
                printf("error ");
                break;
        }
        print_expr(expr->rightOperand);
    }
}

void test_parser( FILE *source )
{
    Declarations *decls;
    Statements *stmts;
    Declaration decl;
    Statement stmt;
    Program program = parser(source);

    decls = program.declarations;

    while(decls != NULL){
        decl = decls->first;
        if(decl.type == Int)
            printf("i ");
        if(decl.type == Float)
            printf("f ");
        printf("%s ",decl.name);
        decls = decls->rest;
    }

    stmts = program.statements;

    while(stmts != NULL){
        stmt = stmts->first;
        if(stmt.type == Print){
            printf("p %s ", stmt.stmt.variable);
        }

        if(stmt.type == Assignment){
            printf("%s = ", stmt.stmt.assign.id);
            print_expr(stmt.stmt.assign.expr);
        }
        stmts = stmts->rest;
    }

}
