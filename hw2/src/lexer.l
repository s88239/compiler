%option noyywrap 
%{
#include <stdio.h>
#include "header.h" 
int linenumber;
symtab * lookup();
symtab * ptr;
void     insertID();
void 	 printSymTab();
/* You need to define for all tokens in C--, here are some examples */
#define RWReturn  0
#define RWTypedef 1
#define RWIf      2
#define RWElse    3
#define RWInt     4
#define RWFloat   5
#define RWFor     6
#define RWStruct  7
#define RWUnion   8
#define RWVoid    9
#define RWWhile   10

#define INT       20
#define FLOAT     21
#define STRING    22
#define COMMENT   23

#define OP_ADD    30
#define OP_SUB    31
#define OP_MUL    32
#define OP_DIV    33

#define OP_LT     40
#define OP_GT     41
#define OP_LTE    42 
#define OP_GTE    43
#define OP_EQ     44
#define OP_NEQ    45

#define OP_AND    50
#define OP_OR     51
#define OP_NOT    52
#define OP_ASSIGN 53

#define MK_LPAREN 60 
#define MK_RPAREN 61 	
#define MK_LBRACK 62
#define MK_RBRACK 63
#define MK_LBRACE 64
#define MK_RBRACE 65
#define DL_COMMA  66
#define DL_SEMICOL 67
#define DL_DOT    68

#define ERROR 		100 

%}

/* Reserved words */
RWReturn  "return"
RWTypedef "typedef"
RWIf      "if"
RWElse    "else"
RWInt     "int"
RWFloat   "float"
RWFor     "for"
RWStruct  "struct"
RWUnion   "union"
RWVoid    "void"
RWWhile   "while"

letter   [A-Za-z]
digit    [0-9]
ID	 {letter}({letter}|{digit}|"_")*
WS	 [ \t\r]+
Int_constant {digit}+

Float_constant {digit}*"."{digit}+
String_constant "\""([^\"\\]*(\\.)*)*"\""
Comment "/*"([^*]|[\r\n]|("*"*([^*/]|[\r\n])))*"*"+"/"

/* You need to define the following RE's 
Float_constant
String_constant
comment
*/

OP_add "+"
OP_sub "-"
OP_mul "*"
OP_div "/"
OP_less "<"
OP_greater ">"
OP_less_equal "<="
OP_greater_equal ">="
OP_equal "=="
OP_not_equal "!="

OP_assign        "="
OP_and "&&"
OP_or   "||"
OP_not "!"

/* Other operators appear here */ 


newline  "\n"

DL_lparen "("
DL_rparen ")"
DL_lbrace "{"
DL_rbrace "}"
DL_lbrack "["
DL_rbrack "]"
DL_comma  ","
DL_semicol ";"
DL_dot  "."

/* Other separators appear here */

error    .

%%

{WS}            {}
{RWReturn}  {}
{RWTypedef} {}
{RWIf}      {}
{RWElse}    {}
{RWInt}     {}
{RWFloat}   {}
{RWFor}     {}
{RWStruct}  {}
{RWUnion}   {}
{RWVoid}    {}
{RWWhile}   {}


{Int_constant} {}
{Float_constant} {}
{String_constant} {}

{OP_add}    {}
{OP_sub}    {}
{OP_mul}    {}
{OP_div}    {}

{OP_less}    {}
{OP_greater}    {}
{OP_less_equal}   {} 
{OP_greater_equal}   {}
{OP_equal}    {}
{OP_not_equal}   {}

{OP_and}    {}
{OP_or}     {}
{OP_not}    {}
{OP_assign} {}

{DL_lparen}     {/* return MK_LPAREN; */}
{DL_rparen}     {/* return MK_RPAREN; */}
{DL_lbrack}		{}
{DL_rbrack}		{}
{DL_lbrace}     {/* return MK_LBRACE; */}
{DL_rbrace}     {/* return MK_RBRACE; */}
{DL_comma}      {}
{DL_semicol}    {}
{DL_dot}		{}
{newline}       linenumber += 1;
{Comment}       printf("%s\n",yytext);
{ID}            { 
			ptr = lookup(yytext);
			if (ptr == NULL)
			     insertID(yytext);	
			else 
			     ptr->counter++;
		}
{error}         {printf("ERR %s\n", yytext); exit(1);}/* return ERROR; */


%%

main(int argc, char **argv)
{
 argc--; ++argv;
   if (argc > 0)
      yyin = fopen(argv[0], "r");
   else
      yyin = stdin;
   yylex();
   printf("Frequency of identifiers:\n");
   printSymTab();
}

