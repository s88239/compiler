#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<math.h>
#include"header.h"

#define TABLE_SIZE	256

symtab * hash_table[TABLE_SIZE];
extern int linenumber;

struct printsymtab{
    symtab* ptr;
};

int compare(const void *a, const void *b){
    struct printsymtab *p1 = (struct printsymtab *)a;
    struct printsymtab *p2 = (struct printsymtab *)b;
    return (strcmp(p1->ptr->lexeme, p2->ptr->lexeme));
}

int HASH(char * str){
	int idx=0;
	while(*str){
		idx = idx << 1;
		idx+=*str;
		str++;
	}	
	return (idx & (TABLE_SIZE-1));
}

/*returns the symbol table entry if found else NULL*/

symtab * lookup(char *name){
	int hash_key;
	symtab* symptr;
	if(!name)
		return NULL;
	hash_key=HASH(name);
	symptr=hash_table[hash_key];

	while(symptr){
		if(!(strcmp(name,symptr->lexeme)))
			return symptr;
		symptr=symptr->front;
	}
	return NULL;
}


void insertID(char *name){
	int hash_key;
	symtab* ptr;
	symtab* symptr=(symtab*)malloc(sizeof(symtab));	
	
	hash_key=HASH(name);
	ptr=hash_table[hash_key];
	
	if(ptr==NULL){
		/*first entry for this hash_key*/
		hash_table[hash_key]=symptr;
		symptr->front=NULL;
		symptr->back=symptr;
	}
	else{
		symptr->front=ptr;
		ptr->back=symptr;
		symptr->back=symptr;
		hash_table[hash_key]=symptr;	
	}
	
	strcpy(symptr->lexeme,name);
	symptr->line=linenumber;
	symptr->counter=1;
}

void printSym(symtab* ptr) 
{
	    printf(" Name = %s \n", ptr->lexeme);
	    printf(" References = %d \n", ptr->counter);
}

void printSymTab()
{
    int i, j, count = 0;
    //printf("----- Symbol Table ---------\n");
    for (i=0; i<TABLE_SIZE; i++)
    {
        symtab* symptr;
		symptr = hash_table[i];
		while (symptr != NULL)
		{
	        //printf("====>  index = %d \n", i);
		    //printSym(symptr);
		    count++;
		    symptr=symptr->front;
		}
    }


    struct printsymtab *print = (struct printsymtab *)malloc(sizeof(struct printsymtab)*count);
    for ( i = 0, j = 0; i < TABLE_SIZE; i++ ){
    	symtab* symptr;
		symptr = hash_table[i];
		while (symptr != NULL)
		{
			print[j].ptr = symptr;
	        symptr=symptr->front;
			j++;
		}
    }

    qsort(print, count, sizeof(struct printsymtab), compare);
    for ( i = 0; i < count; i++ ){
    	printf("%s %i\n", print[i].ptr->lexeme, print[i].ptr->counter);
    }

}
