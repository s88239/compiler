@ In[genProgramNode]
@ In[genDeclarationlNode]
@ In[genFunctionDecl]
@ In[gen_head]
.text
MAIN:
@ Out[gen_head]
@ In[gen_parameter]
@ Out[gen_parameter]
@ In[gen_prologue]: MAIN
_start_MAIN:
	str lr, [sp, #0]
	str fp, [sp, #-4]
	add fp, sp, #-4
	add sp, sp, #-8
	ldr lr, =_framesize_MAIN
	ldr lr, [lr, #0]
	sub sp, sp, lr
	str r4, [sp, #4]
	str r5, [sp, #8]
	str r6, [sp, #12]
	str r7, [sp, #16]
	str r8, [sp, #20]
	str r9, [sp, #24]
	str r10, [sp, #28]
	str r11, [sp, #32]
	vstr.f32 s16, [sp, #36]
	vstr.f32 s17, [sp, #40]
	vstr.f32 s18, [sp, #44]
	vstr.f32 s19, [sp, #48]
	vstr.f32 s20, [sp, #52]
	vstr.f32 s21, [sp, #56]
	vstr.f32 s22, [sp, #60]
	vstr.f32 s23, [sp, #64]
@ Out[gen_prologue]: MAIN
@ In[gen_block]
@ In[genGeneralNode]
@ In[genStmtNode]
@ In[genFunctionCall]
@ Out[genFunctionCall]
@ Out[genStmtNode]
@ In[genStmtNode]
@ In[genFunctionCall]
@ Out[genFunctionCall]
@ Out[genStmtNode]
@ In[genStmtNode]
@ In[genReturn]
@ In[genExpr]
@ In[visitConst]
	ldr r4, =0 
@ Out[visitConst]
@ Out[genExpr]
	mov r0, r4
@ Out[genReturn]
@ Out[genStmtNode]
@ Out[genGeneralNode]
@ Out[gen_block]
@ In[gen_epilogue]: MAIN
_end_MAIN:
	ldr r4, [sp, #4]
	ldr r5, [sp, #8]
	ldr r6, [sp, #12]
	ldr r7, [sp, #16]
	ldr r8, [sp, #20]
	ldr r9, [sp, #24]
	ldr r10, [sp, #28]
	ldr r11, [sp, #32]
	vldr.f32 s16, [sp, #36]
	vldr.f32 s17, [sp, #40]
	vldr.f32 s18, [sp, #44]
	vldr.f32 s19, [sp, #48]
	vldr.f32 s20, [sp, #52]
	vldr.f32 s21, [sp, #56]
	vldr.f32 s22, [sp, #60]
	vldr.f32 s23, [sp, #64]
	ldr lr, [fp, #4]
	mov sp, fp
	add sp, sp, #4
	ldr fp, [fp, #0]
	bx lr

.data
_framesize_MAIN: .word 68

@ Out[gen_epilogue]: MAIN
@ Out[genFunctionDecl]
@ Out[genDeclarationlNode]
@ Out[genProgramNode]
