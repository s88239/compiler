#ifndef CODEGEN_H
#define CODEGEN_H
#include "header.h"

#define debug 0

#define FRAME_SIZE 64
#define FLOAT_POOL_COUNT 256

#define WHILE_LABEL "_While"
#define WHILEEXIT_LABEL "_WhileExit"

#define ELSE_LABEL "_Lelse"
#define ELSEEXIT_LABEL "_LelseExit"

#define FLOAT_POOL "_float_"

#define INT_LABEL "_int_label"
#define FLOAT_LABEL "_float_label"
#define STRING_LABEL "_str_"

void gen_head(char *name);
void gen_parameter(AST_NODE *node);
void gen_block(AST_NODE *node);
void gen_prologue(char *func_name);
void gen_epilogue(char *func_name);

int get_reg();
int get_freg();
void free_reg(int index);
void free_freg(int index);
int get_offset(SymbolTableEntry* entry);

void memToReg(AST_NODE *node, char data_type);
void regToMem(AST_NODE *node, int reg, char data_type);
void intToFloat(AST_NODE *node);

void genFunctionDecl(AST_NODE *node);
void genVariableDecl(AST_NODE *node);
void genWhile(AST_NODE *node);
void genFor(AST_NODE *node);
void genAssign(AST_NODE *node);
void genIf(AST_NODE *node);
void genFunctionCall(AST_NODE *node);
void genReturn(AST_NODE *node);

void genExpr(AST_NODE *node);
void visitExpr(AST_NODE *node);
void visitSymRef(AST_NODE *node);
void visitConst(AST_NODE *node);

void genReadCall(AST_NODE *node);
void genWriteCall(AST_NODE *node);

void genProgramNode(AST_NODE *prog_node); 
void genGeneralNode(AST_NODE *node);
void genDeclarationNode(AST_NODE *decl_node);
void genStmtNode(AST_NODE *node);


void codeGeneration(AST_NODE *root);

#endif