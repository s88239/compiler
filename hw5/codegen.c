#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "symbolTable.h"
#include "codegen.h"

FILE *output;

int ARoffset = -4;
int reg_number = 16;
int reg[16] = {0};
int freg[32] = {0};

int float_pool_count = 0;
char float_pool[FLOAT_POOL_COUNT][128];

int float_count = 0;
int for_count = 0;
int while_count = 0;
int if_count = 0;

int int_label_count = 0;
int float_label_count = 0;
int string_label_count = 0;

void print_float_pool(){
	int i;
	//fprintf(output, ".align 2\n");
	fprintf(output, ".data\n");
	for( i = 0; i < float_pool_count; i++ ){
		fprintf(output, "%s\n", float_pool[i]);
	}
}

int get_reg(){
	int i;
	for ( i = 4; i < 12; i++ ){
		if ( reg[i] == 0 ){
			reg[i] = 1;
			return i;
		}
	}
	return -1;
}

int get_freg(){
	int i;
	for ( i = 16; i < 24; i++ ){
		if ( freg[i] == 0 ){
			freg[i] = 1;
			return i;
		}	
	}
	return -1;
}

void free_reg(int index){
	if ( index >= 0 )
		reg[index] = 0;
}

void free_freg(int index){
	if ( index >= 0 )
		freg[index] = 0;
} 

int get_offset(SymbolTableEntry *entry){
	return entry->offset;
}


void gen_head(char *name){
	fprintf(output, "@ In[gen_head]\n");

	fprintf(output, ".text\n");
	fprintf(output, "%s:\n", name);

	fprintf(output, "@ Out[gen_head]\n");
}

void gen_parameter(AST_NODE *parameter_node){
	fprintf(output, "@ In[gen_parameter]\n");
	int offset = 8;
	AST_NODE *node = parameter_node->child;
	while ( node != NULL ){
		node->child->rightSibling->semantic_value.identifierSemanticValue.symbolTableEntry->offset = offset;
		offset += 4;
		node = node->rightSibling;
	}

	fprintf(output, "@ Out[gen_parameter]\n");
}

void gen_prologue(char *func_name){
	fprintf(output, "@ In[gen_prologue]: %s\n", func_name);

	fprintf(output, "_start_%s:\n", func_name);
	fprintf(output, "\tstr lr, [sp, #0]\n");
	fprintf(output, "\tstr fp, [sp, #-4]\n");
	fprintf(output, "\tadd fp, sp, #-4\n");
	fprintf(output, "\tadd sp, sp, #-8\n");
	fprintf(output, "\tldr lr, =_framesize_%s\n", func_name);
	fprintf(output, "\tldr lr, [lr, #0]\n");
	fprintf(output, "\tsub sp, sp, lr\n");
	fprintf(output, "\tstr r4, [sp, #4]\n");
	fprintf(output, "\tstr r5, [sp, #8]\n");
	fprintf(output, "\tstr r6, [sp, #12]\n");
	fprintf(output, "\tstr r7, [sp, #16]\n");
	fprintf(output, "\tstr r8, [sp, #20]\n");
	fprintf(output, "\tstr r9, [sp, #24]\n");
	fprintf(output, "\tstr r10, [sp, #28]\n");
	fprintf(output, "\tstr r11, [sp, #32]\n");
	
	fprintf(output, "\tvstr.f32 s16, [sp, #36]\n");
	fprintf(output, "\tvstr.f32 s17, [sp, #40]\n");
	fprintf(output, "\tvstr.f32 s18, [sp, #44]\n");
	fprintf(output, "\tvstr.f32 s19, [sp, #48]\n");
	fprintf(output, "\tvstr.f32 s20, [sp, #52]\n");
	fprintf(output, "\tvstr.f32 s21, [sp, #56]\n");
	fprintf(output, "\tvstr.f32 s22, [sp, #60]\n");
	fprintf(output, "\tvstr.f32 s23, [sp, #64]\n");

	fprintf(output, "@ Out[gen_prologue]: %s\n", func_name);
}

void gen_epilogue(char *func_name){
	fprintf(output, "@ In[gen_epilogue]: %s\n", func_name);

	fprintf(output, "_end_%s:\n", func_name);
	fprintf(output, "\tldr r4, [sp, #4]\n");
	fprintf(output, "\tldr r5, [sp, #8]\n");
	fprintf(output, "\tldr r6, [sp, #12]\n");
	fprintf(output, "\tldr r7, [sp, #16]\n");
	fprintf(output, "\tldr r8, [sp, #20]\n");
	fprintf(output, "\tldr r9, [sp, #24]\n");
	fprintf(output, "\tldr r10, [sp, #28]\n");
	fprintf(output, "\tldr r11, [sp, #32]\n");
	
	fprintf(output, "\tvldr.f32 s16, [sp, #36]\n");
	fprintf(output, "\tvldr.f32 s17, [sp, #40]\n");
	fprintf(output, "\tvldr.f32 s18, [sp, #44]\n");
	fprintf(output, "\tvldr.f32 s19, [sp, #48]\n");
	fprintf(output, "\tvldr.f32 s20, [sp, #52]\n");
	fprintf(output, "\tvldr.f32 s21, [sp, #56]\n");
	fprintf(output, "\tvldr.f32 s22, [sp, #60]\n");
	fprintf(output, "\tvldr.f32 s23, [sp, #64]\n");

	fprintf(output, "\tldr lr, [fp, #4]\n");
	fprintf(output, "\tmov sp, fp\n");
	fprintf(output, "\tadd sp, sp, #4\n");
	fprintf(output, "\tldr fp, [fp, #0]\n");
	fprintf(output, "\tbx lr\n");

	fprintf(output, "\n.data\n");
	if ( (FRAME_SIZE - ARoffset -4) % 8 == 0 ){
		fprintf(output, "_framesize_%s: .word %d\n\n", func_name, FRAME_SIZE - ARoffset - 4);
	}
	else{
		fprintf(output, "_framesize_%s: .word %d\n\n", func_name, FRAME_SIZE - ARoffset);
	}

	fprintf(output, "@ Out[gen_epilogue]: %s\n", func_name);
}

void memToReg(AST_NODE *node, char c){
	fprintf(output, "@ In[memToReg]\n");
	if ( node->place < 0 ) {
		int reg;
		if ( c == 'i' ){
			reg = get_reg();
			fprintf(output, "\tldr r%d, [fp, #%d]\n", reg, node->place);
		}
		else if ( c == 'f' ){
			reg = get_freg();
			fprintf(output, "\tvldr.f32 s%d, [fp, #%d]\n", reg, node->place);

		}
		node->place = reg;
	}
	fprintf(output, "@ Out[memToReg]\n");
}

void regToMem(AST_NODE *node, int reg, char c){
	fprintf(output, "@ In[regToMem]\n");
	if ( c == 'i' ){
		fprintf(output, "\tstr r%d, [fp, #%d]\n", reg, node->place);
	}
	else if ( c == 'f' ){
		fprintf(output, "\tvstr.f32 s%d, [fp, #%d]\n", reg, node->place);
	}
	fprintf(output, "@ Out[regToMem]\n");
}

void intToFloat(AST_NODE *node){
	fprintf(output, "@ In[intToFloat]\n");
	if ( node->dataType == INT_TYPE ) {
		memToReg(node, 'i');

		int freg = get_freg();
		if ( freg != -1 ) {
			fprintf(output, "\tvmov.f32 s%d, r%d\n", freg, node->place);
			fprintf(output, "\tvcvt.f32.s32 s%d, s%d\n", freg, freg);
			free_reg(node->place);	
			node->place = freg;
		} 
		else {		
			fprintf(output, "\tvmov.f32 s24, r%d\n", node->place);
			fprintf(output, "\tvcvt.f32.s32 s24, s24\n");
			free_reg(node->place);	
			node->place = ARoffset;
			node->place_type = -1;
			fprintf(output, "\tvstr.f32 s24, [fp, #%d]\n", node->place);
			ARoffset -= 4;
		}
	}
	fprintf(output, "@ Out[intToFloat]\n");
}


void gen_block(AST_NODE *node){
	fprintf(output, "@ In[gen_block]\n");
	if ( node->child->nodeType == VARIABLE_DECL_LIST_NODE ){
		genGeneralNode(node->child);
		if ( node->child->rightSibling != NULL ){
			genGeneralNode(node->child->rightSibling);
		}
	}
	else if ( node->child->nodeType == STMT_LIST_NODE ){
		genGeneralNode(node->child);
	}
	fprintf(output, "@ Out[gen_block]\n");
}


void genProgramNode(AST_NODE *prog_node){
	fprintf(output, "@ In[genProgramNode]\n");
	AST_NODE *node = prog_node->child;
	while ( node != NULL ){
	//fprintf(output, "%d\n", node->nodeType);
		if ( node->nodeType == VARIABLE_DECL_LIST_NODE ){
			genGeneralNode(node);
		}
		else{
			genDeclarationNode(node);
		}

		node = node->rightSibling;
	}
	
	fprintf(output, "@ Out[genProgramNode]\n");
} 

void genGeneralNode(AST_NODE *node){
	fprintf(output, "@ In[genGeneralNode]\n");
	//fprintf(output, "%d\n", node->nodeType);
	AST_NODE *traverseChildren = node->child;
	if ( node->nodeType == VARIABLE_DECL_LIST_NODE ){
		while(traverseChildren){
			genDeclarationNode(traverseChildren);
			traverseChildren = traverseChildren->rightSibling;
		}
	}
	else if ( node->nodeType == STMT_LIST_NODE ) {
		while(traverseChildren){
			genStmtNode(traverseChildren);
			traverseChildren = traverseChildren->rightSibling;
		}
	}
	fprintf(output, "@ Out[genGeneralNode]\n");
}

void genDeclarationNode(AST_NODE *decl_node){
	fprintf(output, "@ In[genDeclarationlNode]\n");
	if ( decl_node->semantic_value.declSemanticValue.kind == VARIABLE_DECL ){
		AST_NODE *node = decl_node->child->rightSibling;
		while ( node!= NULL ){
			genVariableDecl(node);
			node = node->rightSibling;
		}
	}
	else if ( decl_node->semantic_value.declSemanticValue.kind == FUNCTION_DECL ){
		genFunctionDecl(decl_node);
	}
	/*else if ( decl_node->semantic_value.declSemanticValue.kind == TYPE_DECL ){

	}
	else if ( decl_node->semantic_value.declSemanticValue.kind == FUNCTION_PARAMETER_DECL ){
		
	}*/
	fprintf(output, "@ Out[genDeclarationlNode]\n");
}

void genStmtNode(AST_NODE *node){
	fprintf(output, "@ In[genStmtNode]\n");
	if( node->nodeType == NUL_NODE ){
        return;
    }
    else if ( node->nodeType == BLOCK_NODE ){
    	gen_block(node);
    }
    else{
    	if ( node->semantic_value.stmtSemanticValue.kind == WHILE_STMT ){
    		genWhile(node);
    	}
    	else if ( node->semantic_value.stmtSemanticValue.kind == FOR_STMT ){
    		genFor(node);
    	}
    	else if ( node->semantic_value.stmtSemanticValue.kind == ASSIGN_STMT ){
    		genAssign(node);
    	}
    	else if ( node->semantic_value.stmtSemanticValue.kind == IF_STMT ){
    		genIf(node);
    	}
    	else if ( node->semantic_value.stmtSemanticValue.kind == FUNCTION_CALL_STMT ){
    		genFunctionCall(node);
    	}
    	else if ( node->semantic_value.stmtSemanticValue.kind == RETURN_STMT ){
    		genReturn(node);
    	}
    }

	fprintf(output, "@ Out[genStmtNode]\n");
}



void genVariableDecl(AST_NODE *node){
	fprintf(output, "@ In[genVariableDecl]\n");
	if ( node->semantic_value.identifierSemanticValue.kind == NORMAL_ID ){
		SymbolTableEntry *entry = node->semantic_value.identifierSemanticValue.symbolTableEntry;
		if ( entry->nestingLevel == 0){ 
			fprintf(output, ".data\n");
			if ( entry->attribute->attr.typeDescriptor->properties.dataType == INT_TYPE ){
				fprintf(output, "_%s: .word 0\n", node->semantic_value.identifierSemanticValue.identifierName);
			}
			if( entry->attribute->attr.typeDescriptor->properties.dataType == FLOAT_TYPE ){
				fprintf(output, "_%s: .float 0.0\n", node->semantic_value.identifierSemanticValue.identifierName);	
			}
		}
		else{	
			entry->offset = ARoffset;
			ARoffset -= 4;
		}
	}
	else if ( node->semantic_value.identifierSemanticValue.kind == ARRAY_ID ){
		SymbolTableEntry *entry = node->semantic_value.identifierSemanticValue.symbolTableEntry;
		int size = 4;
		int i;
		ArrayProperties property = entry->attribute->attr.typeDescriptor->properties.arrayProperties;
		for ( i = 0; i < property.dimension; i++ ){
			size *= property.sizeInEachDimension[i];
		}

		if ( entry->nestingLevel == 0 ){
			fprintf(output, ".data\n");
			fprintf(output, "_%s: .space %d\n", node->semantic_value.identifierSemanticValue.identifierName, size);
		}
		else{
			ARoffset -= (size - 4);
			entry->offset = ARoffset;
			ARoffset -= 4;
		}
	
	}
	else if ( node->semantic_value.identifierSemanticValue.kind == WITH_INIT_ID ){
		SymbolTableEntry *entry = node->semantic_value.identifierSemanticValue.symbolTableEntry;
		AST_NODE *const_value = node->child;
		CON_Type *value = const_value->semantic_value.const1;
		if ( entry->attribute->attr.typeDescriptor->properties.dataType == INT_TYPE ){
			if ( entry->nestingLevel == 0 ){
				fprintf(output, ".data\n");
				if (value->const_type == INTEGERC) {
					fprintf(output, "_%s: .word %d\n", node->semantic_value.identifierSemanticValue.identifierName, value->const_u.intval);
				} 
				else if(value->const_type == FLOATC) {
					fprintf(output, "_%s: .word %d\n", node->semantic_value.identifierSemanticValue.identifierName, (int)value->const_u.fval);
				}
			}
			else{
				int r = get_reg();
				/*if ( r == -1 ){

				}*/
				entry->offset = ARoffset;
				ARoffset -= 4;
				if ( value->const_type == INTEGERC ) {
					fprintf(output, "\tldr r%d, =%d\n", r, value->const_u.intval);
				} 
				else if( value->const_type == FLOATC ) {
					fprintf(output, "\tldr r%d, =%d\n", r, (int)value->const_u.fval);
				}
				fprintf(output, "\tstr r%d, [fp, #%d]\n", r, entry->offset);
				free_reg(r);
			}
		}
		else if ( entry->attribute->attr.typeDescriptor->properties.dataType == FLOAT_TYPE ){
			if ( entry->nestingLevel == 0 ){
				fprintf(output, ".data\n");
				if ( value->const_type == INTEGERC ) {
					fprintf(output, "_%s: .float %d.0\n", node->semantic_value.identifierSemanticValue.identifierName, value->const_u.intval);
				} 
				else if( value->const_type == FLOATC ) {
					fprintf(output, "_%s: .float %f\n", node->semantic_value.identifierSemanticValue.identifierName, value->const_u.fval);
				}
			}
			else{
				int f = get_freg();
				if( f == -1 ){
				}
				if( float_pool_count == FLOAT_POOL_COUNT ){
				}
				if ( value->const_type == INTEGERC ) { 
					/*fprintf(output, ".data\n");
					fprintf(output, FLOAT_LABEL"%d: .float %d.0\n", float_label_count, value->const_u.intval);
					fprintf(output, ".text\n");*/
					sprintf( float_pool[float_pool_count], FLOAT_POOL"%d: .float %d.0", float_pool_count, value->const_u.intval);
				} 
				else if( value->const_type == FLOATC ) {
					/*fprintf(output, ".data\n");
					fprintf(output, FLOAT_LABEL"%d: .float %f\n", float_label_count, value->const_u.fval);
					fprintf(output, ".text\n");*/
					sprintf( float_pool[float_pool_count], FLOAT_POOL"%d: .float %f", float_pool_count, value->const_u.fval);
				}

				int tmp = get_reg();
				fprintf(output, "\tldr r%d, =" FLOAT_POOL "%d\n", tmp, float_pool_count);
				fprintf(output, "\tvldr.f32 s%d, [r%d, #0]\n", f, tmp);
				float_pool_count++;

				entry->offset = ARoffset;
				ARoffset -= 4;
				fprintf(output, "\tvstr.f32 s%d, [fp, #%d]\n", f, entry->offset);
				//fprintf(output, "\tstr r%d, [fp, #%d]\n", tmp, entry->offset);
				free_freg(f);
				free_reg(tmp);
				
				//label_count++;
				//float_count++;
			}

		}
	}
	fprintf(output, "@ Out[genVariableDecl]\n");
}

void genFunctionDecl(AST_NODE *func_node){
	fprintf(output, "@ In[genFunctionDecl]\n");
	ARoffset = -4;
	AST_NODE *node = func_node->child->rightSibling;
	char *func_name = node->semantic_value.identifierSemanticValue.identifierName;
	gen_head(func_name);
	gen_parameter(node->rightSibling);
	gen_prologue(func_name);
	gen_block(node->rightSibling->rightSibling);
	gen_epilogue(func_name);
	fprintf(output, "@ Out[genFunctionDecl]\n");
}

void genWhile(AST_NODE *node){
	fprintf(output, "@ In[genWhile]\n");
	int count = while_count;
	while_count++;

	AST_NODE *bool_expr = node->child;
	AST_NODE *body_node = node->child->rightSibling;
	fprintf(output, WHILE_LABEL"%d:\n", count);
	genExpr(bool_expr);
	fprintf(output, "\tcmp r%d, #0\n", bool_expr->place);
	fprintf(output, "\tbeq " WHILEEXIT_LABEL"%d\n", count);
	genStmtNode(body_node);
	fprintf(output, "\tb " WHILE_LABEL"%d\n", count);
	fprintf(output, WHILEEXIT_LABEL"%d:\n", count);
	fprintf(output, "@ Out[genWhile]\n");
}

void genFor(AST_NODE *node){
	fprintf(output, "@ In[genFor]\n");

	fprintf(output, "@ Out[genFor]\n");
}

void genAssign(AST_NODE *node){
	fprintf(output, "@ In[genAssign]\n");
	AST_NODE* left = node->child;
	AST_NODE* right = node->child->rightSibling;
	genExpr(right);
	if( left->dataType == FLOAT_TYPE ){
		intToFloat(right);
	}	
	char* var_name = left->semantic_value.identifierSemanticValue.identifierName;
	SymbolTableEntry *entry = left->semantic_value.identifierSemanticValue.symbolTableEntry;
	int global = (entry->nestingLevel == 0);

	if ( left->semantic_value.identifierSemanticValue.kind == NORMAL_ID ){
		if ( left->dataType == INT_TYPE ){
			if ( global ){
				int reg = get_reg();
				fprintf(output, "\tldr r%d, =_%s\n", reg, var_name);
				if( right->place > 0 && right->place_type != -1 ){
					fprintf(output, "\tstr r%d, [r%d, #0]\n", right->place, reg);
				}
				else{
					memToReg(right, 'i');
					fprintf(output, "\tstr r%d, [r%d, #0]\n", right->place, reg);
				}
				free_reg(reg);
			}
			else{
				if( right->place > 0 && right->place_type != -1 )
					fprintf(output, "\tstr  r%d, [fp, #%d]\n", right->place, entry->offset);
				else{
					memToReg(right, 'i');
					fprintf(output, "\tstr  r%d, [fp, #%d]\n", right->place, entry->offset);
				}
			}
			free_reg(right->place);
		}
		else if ( left->dataType == FLOAT_TYPE ){
			if ( global ){
				int reg = get_reg();
				fprintf(output, "\tldr r%d, =_%s\n", reg, var_name);
				
				if( right->place > 0 && right->place_type != -1 ){
					//fprintf(output, "\tvstr.f32  s%d, _%s\n", right->place, var_name);
					fprintf(output, "\tvstr.f32 s%d, [r%d, #0]\n", right->place, reg);
					fprintf(output, "\tvldr.f32 s%d, [r%d]\n", right->place, reg);
					fprintf(output, "\tvstr.f32 s%d, [r%d, #0]\n", right->place, reg);
				}
				else{
					memToReg(right, 'f');
					fprintf(output, "\tvstr.f32 s%d, [r%d, #0]\n", right->place, reg);
					fprintf(output, "\tvldr.f32 s%d, [r%d]\n", right->place, reg);
					fprintf(output, "\tvstr.f32 s%d, [r%d, #0]\n", right->place, reg);
				}
				free_reg(reg);
			}
			else{
				if( right->place > 0 && right->place_type != -1 )
					fprintf(output, "\tvstr.f32  s%d, [fp, #%d]\n", right->place, entry->offset);
				else{
					memToReg(right, 'f');
					fprintf(output, "\tvstr.f32  s%d, [fp, #%d]\n", right->place, entry->offset);
				}
			}
			free_freg(right->place);
		}
	}
	else if ( left->semantic_value.identifierSemanticValue.kind == ARRAY_ID ){
		ArrayProperties property = entry->attribute->attr.typeDescriptor->properties.arrayProperties;

		int current_dimension = 0;
		AST_NODE* current_dimension_node = left->child;
		int total_offset_reg = get_reg();
		if (total_offset_reg != -1) {
		} 
		else {
			total_offset_reg = 12;
		}
		fprintf(output, "\tldr  r%d, =0\n", total_offset_reg);
		while(current_dimension_node != NULL){
			genExpr(current_dimension_node);
			if( current_dimension_node->place > 0 && current_dimension_node->place_type != -1 )
				fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, current_dimension_node->place);
			else{
				memToReg(current_dimension_node, 'i');
				fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, current_dimension_node->place);
			}
			free_reg(current_dimension_node->place);
			if ( current_dimension_node->rightSibling != NULL ) {
				int reg = get_reg();
				if (reg == -1){
					reg = 12;
				}
				fprintf(output, "\tldr r%d, =%d\n", reg, property.sizeInEachDimension[current_dimension+1]);
				fprintf(output, "\tmul r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, reg);
				free_reg(reg);
			} 
			else {
				int tmp = get_reg();
				fprintf(output, "\tldr r%d, =4\n", tmp);
				fprintf(output, "\tmul r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, tmp);
				free_reg(tmp);
			}
			current_dimension += 1;
			current_dimension_node = current_dimension_node->rightSibling;
		}
		if ( left->dataType == INT_TYPE ){
			if ( global ){
				int tmp = get_reg();
				if ( tmp == -1 ){
					tmp = 12;
				}
				if( right->place > 0 && right->place_type != -1 ){
					fprintf(output, "\tldr r%d, =_%s\n", tmp, var_name);
					//fprintf(output, "\tadd r%d, r%d, r%d\n", tmp, tmp, entry->offset);
					fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, tmp);
					fprintf(output, "\tstr r%d, [r%d, #%d]\n", right->place, total_offset_reg, entry->offset);
				}
				else{
					memToReg(right, 'i');
					fprintf(output, "\tldr r%d, =_%s\n", tmp, var_name);
					//fprintf(output, "\tadd r%d, r%d, r%d\n", tmp, tmp, entry->offset);
					fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, tmp);
					fprintf(output, "\tstr r%d, [r%d, #%d]\n", right->place, total_offset_reg, entry->offset);
				}
				free_reg(tmp);
			}
			else{
				if( right->place > 0 && right->place_type != -1 ){
					fprintf(output, "\tadd r%d, r%d, fp\n",  total_offset_reg, total_offset_reg);
					fprintf(output, "\tadd r%d, r%d, #%d\n",  total_offset_reg, total_offset_reg, entry->offset);
					fprintf(output, "\tstr r%d, [r%d, #0]\n",  right->place, total_offset_reg);
				}
				else{
					memToReg(right, 'i');
					fprintf(output, "\tadd r%d, r%d, fp\n",  total_offset_reg, total_offset_reg);
					fprintf(output, "\tadd r%d, r%d, #%d\n",  total_offset_reg, total_offset_reg, entry->offset);
					fprintf(output, "\tstr r%d, [r%d, #0]\n",  right->place, total_offset_reg);
				}
			}
			free_reg(right->place);
		}
		else if ( left->dataType == FLOAT_TYPE ){
			if ( global ){
				int tmp = get_reg();
				if ( tmp == -1 ){
					tmp = 12;
				}
				if( right->place > 0 && right->place_type != -1 ){
					fprintf(output, "\tldr r%d, =_%s\n", tmp, var_name);
					//fprintf(output, "\tadd r%d, r%d, r%d\n", tmp, tmp, entry->offset);
					fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, tmp);
					fprintf(output, "\tvstr.f32 s%d, [r%d, #%d]\n", right->place, total_offset_reg, entry->offset);
				}
				else{
					memToReg(right, 'f');
					fprintf(output, "\tldr r%d, =_%s\n", tmp, var_name);
					//fprintf(output, "\tadd r%d, r%d, r%d\n", tmp, tmp, entry->offset);
					fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, tmp);
					fprintf(output, "\tvstr.f32 s%d, [r%d, #%d]\n", right->place, total_offset_reg, entry->offset);
				}
				free_reg(tmp);
			}
			else{
				if( right->place > 0 && right->place_type != -1 ){
					fprintf(output, "\tadd r%d, r%d, fp\n",  total_offset_reg, total_offset_reg);
					fprintf(output, "\tadd r%d, r%d, #%d\n",  total_offset_reg, total_offset_reg, entry->offset);
					fprintf(output, "\tvstr.f32 s%d, [r%d, #0]\n",  right->place, total_offset_reg);
				}
				else{
					memToReg(right, 'i');
					fprintf(output, "\tadd r%d, r%d, fp\n",  total_offset_reg, total_offset_reg);
					fprintf(output, "\tadd r%d, r%d, #%d\n",  total_offset_reg, total_offset_reg, entry->offset);
					fprintf(output, "\tvstr.f32 s%d, [r%d, #0]\n",  right->place, total_offset_reg);
				}
			}
			free_freg(right->place);
		}
		free_reg(total_offset_reg);
	}

	fprintf(output, "@ Out[genAssign]\n");
}

void genIf(AST_NODE *node){
	fprintf(output, "@ In[genIf]\n");
	int count = if_count;
	if_count++;

	AST_NODE* bool_expr = node->child;
	AST_NODE* if_stmt = bool_expr->rightSibling;
	AST_NODE* else_stmt = if_stmt->rightSibling;

	genExpr(bool_expr);
	if ( else_stmt != NULL ){
		if ( bool_expr->place < 16 ){	
			fprintf(output, "\tcmp r%d, #0\n", bool_expr->place);
			//fprintf(output, "\tbeq r%d," ELSE_LABEL "%d\n", bool_expr->place, count);
		}
		else{
			fprintf(output, "\tvcmp.f32 s%d, #0\n", bool_expr->place);
			fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
			//fprintf(output, "\tbeq s%d," ELSE_LABEL "%d\n", bool_expr->place, count);
		}
			fprintf(output, "\tbeq " ELSE_LABEL "%d\n", count);
			genStmtNode(if_stmt);
			fprintf(output, "\tb " ELSEEXIT_LABEL "%d\n", count);

			fprintf(output, ELSE_LABEL"%d:\n", count);
			genStmtNode(else_stmt);

			fprintf(output, ELSEEXIT_LABEL"%d:\n", count);
	}
	else{
		if ( bool_expr->place < 16 ){	
			fprintf(output, "\tcmp r%d, #0\n", bool_expr->place);
		}
		else{
			fprintf(output, "\tvcmp.f32 s%d, #0\n", bool_expr->place);
			fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
		}
		fprintf(output, "\tbeq " ELSEEXIT_LABEL "%d\n", count);

		genStmtNode(if_stmt);
		fprintf(output, "\tb " ELSEEXIT_LABEL "%d\n", count);
	}
	free_reg(bool_expr->place);

	fprintf(output, "@ Out[genIf]\n");
}

void genFunctionCall(AST_NODE *node){
	fprintf(output, "@ In[genFunctionCall]\n");
	AST_NODE* func_name_node = node->child;
	AST_NODE* func_param = node->child->rightSibling;
	char* func_name = func_name_node->semantic_value.identifierSemanticValue.identifierName;
	if ( strcmp(func_name, "read") == 0 || strcmp(func_name, "fread") == 0 ){
		genReadCall(node);
	}
	else if ( func_param->nodeType == NUL_NODE ){
		if ( node->dataType == INT_TYPE ){
			int reg = get_reg();
			if ( reg != -1 ){
				node->place = reg;
				fprintf(output, "\tbl %s\n", func_name);
				fprintf(output, "\tmov r%d, r0\n", reg);
			}
			else{
				node->place = ARoffset;
				node->place_type = -1;
				ARoffset -= 4;
				fprintf(output, "\tbl %s\n", func_name);
				fprintf(output, "\tmov r12, r0\n");
				fprintf(output, "\tstr r12, [fp, #%d]\n", node->place);
				regToMem(node, 12, 'i');
			}
		}
		else if ( node->dataType == FLOAT_TYPE ){
			int freg = get_freg();
			if ( freg != -1 ){
				node->place = freg;
				fprintf(output, "\tbl %s\n", func_name);
				fprintf(output, "\tvmov.f32 s%d, s0\n", freg);
			}
			else{
				node->place = ARoffset;
				node->place_type = -1;
				ARoffset -= 4;
				fprintf(output, "\tbl %s\n", func_name);
				fprintf(output, "\tvmov.f32 s24, s0\n");
				fprintf(output, "\tvstr.f32 s24, [fp, #%d]\n", node->place);
				regToMem(node, 24, 'f');
			}
		}
		else{
			fprintf(output, "bl %s\n", func_name);
		}	
	}
	else if ( func_param->nodeType == NONEMPTY_RELOP_EXPR_LIST_NODE ){
		if ( strcmp(func_name, "write") == 0 ){
			genWriteCall(func_param->child);
		}
		else{
			SymbolTableEntry *entry = retrieveSymbol(func_name);
			Parameter *current_paramter = entry->attribute->attr.functionSignature->parameterList;
			AST_NODE* param = func_param->child;
			int param_offset = 0;
			int param_num = func_name_node->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.functionSignature->parametersCount;
			fprintf(output,	"\tldr r4, =%d\n", param_num * 4);
			fprintf(output, "\tsub sp, sp, r4\n");
			while( param != NULL ){
				genExpr(param);
				param_offset += 4;
				if( current_paramter->type->properties.dataType == INT_TYPE ){

					if( param->place > 0 && param->place_type != -1){
						fprintf(output, "\tstr r%d, [sp, #%d]\n", param->place, param_offset);
					}
					else{
						memToReg(param, 'i');
						fprintf(output, "\tstr r%d, [sp, #%d]\n", param->place, param_offset);
					}
					free_reg(param->place);
				}
				else if( current_paramter->type->properties.dataType == FLOAT_TYPE ){
					intToFloat(param);
					if( param->place >0 && param->place_type != -1){
						fprintf(output, "\tstr s%d, [sp, #%d]\n", param->place, param_offset);
					}
					else{
						memToReg(param, 'f');
						fprintf(output, "\tvstr.f32 s%d, [sp, #%d]\n", param->place, param_offset);
					}
					free_freg(param->place);
				}
				param = param->rightSibling;	
				current_paramter = current_paramter->next;
			}
			if ( node->dataType == INT_TYPE ){
				int reg = get_reg();
				if ( reg != -1 ){
					node->place = reg;
					fprintf(output, "\tbl %s\n", func_name);
					fprintf(output, "\tmov r%d, r0\n", reg);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tbl %s\n", func_name);
					fprintf(output, "\tmov r12, r0\n");
					fprintf(output, "\tstr r12, [fp, #%d]\n", node->place);
					regToMem(node, 12, 'i');
				}
			}
			else if ( node->dataType == FLOAT_TYPE ){
				int freg = get_freg();
				if ( freg != -1 ){
					node->place = freg;
					fprintf(output, "\tbl %s\n", func_name);
					fprintf(output, "\tvmov.f32 s%d, s0\n", freg);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tbl %s\n", func_name);
					fprintf(output, "\tvmov.f32 s24, s0\n");
					fprintf(output, "\tvstr.f32 s24, [fp, #%d]\n", node->place);
					regToMem(node, 24, 'f');
				}
			}	
			else{
				fprintf(output, "\tbl %s\n", func_name);
			}
			fprintf(output,	"\tldr r4, =%d\n", param_offset);
			fprintf(output, "\tadd sp, sp, r4\n");
		}

	}


	fprintf(output, "@ Out[genFunctionCall]\n");
}

void genReturn(AST_NODE *node){
	fprintf(output, "@ In[genReturn]\n");
	AST_NODE* current_node = node;
	while(current_node->nodeType != DECLARATION_NODE) {
		current_node = current_node->parent;
	}
	char* func_name = current_node->child->rightSibling->semantic_value.identifierSemanticValue.identifierName;
	SymbolTableEntry* func_entry = retrieveSymbol(func_name);
	DATA_TYPE returnType = func_entry->attribute->attr.functionSignature->returnType;

	AST_NODE* relop_expr = node->child;
	genExpr(relop_expr);
	int reg;

	if( returnType == INT_TYPE ){
		if ( relop_expr->place > 0 && relop_expr->place_type != -1 ) {
			reg = relop_expr->place;
		} 
		else {
			memToReg(relop_expr, 'i');
			reg = relop_expr->place;
		}
		fprintf(output, "\tmov r0, r%d\n", reg);
		free_reg(reg);
	}
	else if( returnType == FLOAT_TYPE ){
		intToFloat(relop_expr);
		if ( relop_expr->place > 0 && relop_expr->place_type != -1 ) {
			reg = relop_expr->place;
		} 
		else {
			memToReg(relop_expr, 'f');
			reg = relop_expr->place;
		}
		fprintf(output, "\tvmov.f32 s0, s%d\n", reg);
		free_freg(reg);
	}

	fprintf(output, "@ Out[genReturn]\n");
}


void genExpr(AST_NODE *node){
	fprintf(output, "@ In[genExpr]\n");
	if ( node->nodeType == EXPR_NODE ){
		visitExpr(node);
	}
	else if ( node->nodeType == IDENTIFIER_NODE ){
		visitSymRef(node);
	}
	else if ( node->nodeType == CONST_VALUE_NODE ){
		visitConst(node);
	}
	else if ( node->nodeType == STMT_NODE ){
		genFunctionCall(node);
	}
	fprintf(output, "@ Out[genExpr]\n");
}

void visitExpr(AST_NODE *node){
	fprintf(output, "@ In[visitExpr]\n");
	AST_NODE *left = node->child;
	AST_NODE *right = node->child->rightSibling;
	if ( node->dataType == INT_TYPE ){
		int reg;
		reg = get_reg();
		if ( reg != -1 )
			node->place = reg;
		else
			reg = 12;
		if ( node->semantic_value.exprSemanticValue.kind == BINARY_OPERATION ){
			if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_AND ){
				genExpr(left);
				memToReg(left,'i');
				int count = int_label_count;
				int_label_count++;
				fprintf(output, "\tldr r%d, =0\n", reg);
				fprintf(output, "\tcmp r%d, #0\n", left->place);
				fprintf(output, "\tbeq " INT_LABEL "%d\n", count);
				genExpr(right);
				memToReg(right, 'i');
				fprintf(output, "\tcmp r%d, #0\n", right->place);
				fprintf(output, "\tbeq " INT_LABEL "%d\n", count);
				fprintf(output, "\tldr r%d, =1\n", reg);
				fprintf(output, INT_LABEL"%d:\n", count);
			}  
			else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_OR ){
				genExpr(left);
				memToReg(left, 'i');
				int count = int_label_count;
				int_label_count++;
				fprintf(output, "\tldr r%d, =1\n", reg);
				fprintf(output, "\tcmp r%d, #0\n", left->place);
				fprintf(output, "\tbne " INT_LABEL "%d\n", count);
				genExpr(right);
				memToReg(right, 'i');
				fprintf(output, "\tcmp r%d, #0\n", right->place);
				fprintf(output, "\tbne " INT_LABEL "%d\n", count);
				fprintf(output, "\tldr  r%d, =0\n", reg);
				fprintf(output, INT_LABEL"%d:\n", count);
			} 
			else{
				
				genExpr(left);
				genExpr(right);
				memToReg(left, 'i');
				memToReg(right, 'i');
				
				if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_ADD ){
					fprintf(output, "\tadd  r%d, r%d, r%d\n", reg, left->place, right->place);
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_SUB ){
					fprintf(output, "\tsub  r%d, r%d, r%d\n", reg, left->place, right->place);
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_MUL ){
					fprintf(output, "\tmul  r%d, r%d, r%d\n", reg, left->place, right->place);
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_DIV ){
					fprintf(output, "\tsdiv  r%d, r%d, r%d\n", reg, left->place, right->place);
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_EQ ){
					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tcmp r%d, r%d\n", left->place, right->place);
					fprintf(output, "\tbeq " INT_LABEL "%d\n", int_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, INT_LABEL"%d:\n", int_label_count);
					int_label_count++;
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_GE ){
					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tcmp r%d, r%d\n", left->place, right->place);
					fprintf(output, "\tbge " INT_LABEL "%d\n", int_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, INT_LABEL"%d:\n", int_label_count);
					int_label_count++;
				} 
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_LE ){
					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tcmp r%d, r%d\n", left->place, right->place);
					fprintf(output, "\tble " INT_LABEL "%d\n", int_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, INT_LABEL"%d:\n", int_label_count);
					int_label_count++;
					//fprintf(output, "\tle  r%d, r%d, r%d\n", reg, left->place, right->place);
				} 
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_NE ){
					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tcmp r%d, r%d\n", left->place, right->place);
					fprintf(output, "\tbne " INT_LABEL "%d\n", int_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, INT_LABEL"%d:\n", int_label_count);
					int_label_count++;
					//fprintf(output, "\tne  r%d, r%d, r%d\n", reg, left->place, right->place);
				} 
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_GT ){
					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tcmp r%d, r%d\n", left->place, right->place);
					fprintf(output, "\tbgt " INT_LABEL "%d\n", int_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, INT_LABEL"%d:\n", int_label_count);
					int_label_count++;
					//fprintf(output, "\tgt  r%d, r%d, r%d\n", reg, left->place, right->place);
				} 
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_LT ){
					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tcmp r%d, r%d\n", left->place, right->place);
					fprintf(output, "\tblt " INT_LABEL "%d\n", int_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, INT_LABEL"%d:\n", int_label_count);
					int_label_count++;
					//fprintf(output, "\tlt  r%d, r%d, r%d\n", reg, left->place, right->place);
				} 
			}
			free_reg(left->place);
			free_reg(right->place);
		}
		else if ( node->semantic_value.exprSemanticValue.kind == UNARY_OPERATION ){
			genExpr(left);
			memToReg(left, 'i');
			if ( node->semantic_value.exprSemanticValue.op.unaryOp == UNARY_OP_POSITIVE ){
				fprintf(output, "\tmov  r%d, r%d\n", reg, left->place);
			}
			else if ( node->semantic_value.exprSemanticValue.op.unaryOp == UNARY_OP_NEGATIVE ){
				fprintf(output, "\tneg  r%d, r%d\n", reg, left->place);
			}
			else if ( node->semantic_value.exprSemanticValue.op.unaryOp == UNARY_OP_LOGICAL_NEGATION ){
				fprintf(output, "\tldr r%d, =1\n", reg);
				fprintf(output, "\tcmp r%d, #0\n", left->place);
				fprintf(output, "\tbeq " INT_LABEL "%d\n", int_label_count);
				
				fprintf(output, "\tldr r%d, =0\n", reg);
				fprintf(output, INT_LABEL"%d:\n", int_label_count);
				int_label_count++;
				
				//fprintf(output, "\tmov r%d #0\n", tmp);
				//fprintf(output, "\teor r%d, r%d, r%d\n", reg, left->place, tmp);
			}
			free_reg(left->place);
		}
		if ( reg == 12 ) {
			node->place = ARoffset;
			node->place_type = -1;
			regToMem(node, reg, 'i');
			ARoffset -= 4;
		}
	}
	else if ( node->dataType == FLOAT_TYPE ){
		int reg = get_freg();
		if ( reg != -1 ){
			node->place = reg;
		} 
		else{
			reg = 24;
		}
		if ( node->semantic_value.exprSemanticValue.kind == BINARY_OPERATION ){
			if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_AND ){
				free_freg(reg);
				reg = get_reg();
				if ( reg !=-1 ){
					node->place = reg;
				}
				else{
					reg = 12;
				}

				genExpr(left);
				intToFloat(left);
				memToReg(left,'f');
				int count = float_label_count;
				float_label_count++;
				
				fprintf(output, "\tldr r%d, =0\n", reg);
				//fprintf(output, "\tvcvt.s32.f32 r%d, s%d\n", tmp, reg);
				fprintf(output, "\tvcmp.f32 s%d, #0\n", left->place);
				fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
				fprintf(output, "\tbeq " FLOAT_LABEL "%d\n", count);
				genExpr(right);
				intToFloat(right);
				memToReg(right, 'f');
				fprintf(output, "\tvcmp.f32 s%d, #0\n", right->place);
				fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
				fprintf(output, "\tbeq " FLOAT_LABEL "%d\n", count);
				fprintf(output, "\tldr r%d, =1\n", reg);
				fprintf(output, FLOAT_LABEL"%d:\n", count);
			}  
			else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_OR ){
				free_freg(reg);
				reg = get_reg();
				if ( reg !=-1 ){
					node->place = reg;
				}
				else{
					reg = 12;
				}

				genExpr(left);
				intToFloat(left);
				memToReg(left, 'f');
				int count = float_label_count;
				float_label_count++;

				fprintf(output, "\tldr r%d, =1\n", reg);
				fprintf(output, "\tvcmp.f32 s%d, #0\n", left->place);
				fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
				fprintf(output, "\tbne " FLOAT_LABEL "%d\n", count);
				genExpr(right);
				intToFloat(right);
				memToReg(right, 'f');
				fprintf(output, "\tvcmp.f32 s%d, #0\n", right->place);
				fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
				fprintf(output, "\tbne " FLOAT_LABEL "%d\n", count);
				fprintf(output, "\tldr r%d, =0\n", reg);
				fprintf(output, FLOAT_LABEL"%d:\n", count);
			} 
			else{
				genExpr(left);
				genExpr(right);
				intToFloat(left);
				intToFloat(right);
				memToReg(left, 'f');
				memToReg(right, 'f');
				if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_ADD ){
					fprintf(output, "\tvadd.f32 s%d, s%d, s%d\n", reg, left->place, right->place);
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_SUB ){
					fprintf(output, "\tvsub.f32 s%d, s%d, s%d\n", reg, left->place, right->place);
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_MUL ){
					fprintf(output, "\tvmul.f32 s%d, s%d, s%d\n", reg, left->place, right->place);
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_DIV ){
					fprintf(output, "\tvdiv.f32 s%d, s%d, s%d\n", reg, left->place, right->place);
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_EQ ){
					free_freg(reg);
					reg = get_reg();
					if ( reg != -1 ){
						node->place = reg;
					}
					else{
						reg = 12;
					}

					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tvcmp.f32 s%d, s%d\n", left->place, right->place);
					fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
					fprintf(output, "\tbeq " FLOAT_LABEL "%d\n", float_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, FLOAT_LABEL"%d:\n", float_label_count);
					float_label_count++;
					//fprintf(output, "\tvceq.f32 r%d, s%d, s%d\n", reg, left->place, right->place);
				}
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_GE ){
					free_freg(reg);
					reg = get_reg();
					if ( reg != -1 ){
						node->place = reg;
					}
					else{
						reg = 12;
					}

					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tvcmp.f32 s%d, s%d\n", left->place, right->place);
					fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
					fprintf(output, "\tbge " FLOAT_LABEL "%d\n", float_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, FLOAT_LABEL"%d:\n", float_label_count);
					float_label_count++;
					//fprintf(output, "\tvcge.f32 r%d, s%d, s%d\n", reg, left->place, right->place);
				} 
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_LE ){
					free_freg(reg);
					reg = get_reg();
					if ( reg != -1 ){
						node->place = reg;
					}
					else{
						reg = 12;
					}

					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tvcmp.f32 s%d, s%d\n", left->place, right->place);
					fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
					fprintf(output, "\tble " FLOAT_LABEL "%d\n", float_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, FLOAT_LABEL"%d:\n", float_label_count);
					float_label_count++;
					//fprintf(output, "\tvcle.f32 r%d, s%d, s%d\n", reg, left->place, right->place);
				} 
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_NE ){
					free_freg(reg);
					reg = get_reg();
					if ( reg != -1 ){
						node->place = reg;
					}
					else{
						reg = 12;
					}

					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tvcmp.f32 s%d, s%d\n", left->place, right->place);
					fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
					fprintf(output, "\tbne " FLOAT_LABEL "%d\n", float_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, FLOAT_LABEL"%d:\n", float_label_count);
					float_label_count++;
					//fprintf(output, "\tvceq.f32 r%d, s%d, s%d\n", reg, left->place, right->place);
					//fprintf(output, "\teor  r%d, r%d, #0\n", reg, reg);
				} 
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_GT ){
					free_freg(reg);
					reg = get_reg();
					if ( reg != -1 ){
						node->place = reg;
					}
					else{
						reg = 12;
					}

					fprintf(output, "\tldr r%d, =1\n", reg);
					//fprintf(output, "\tvldr.f32 s%d, =1\n", reg);
					fprintf(output, "\tvcmp.f32 s%d, s%d\n", left->place, right->place);
					fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
					fprintf(output, "\tbgt " FLOAT_LABEL "%d\n", float_label_count);
					//fprintf(output, "\tvldr.f32 s%d, =0\n", reg);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, FLOAT_LABEL"%d:\n", float_label_count);
					float_label_count++;				
					//fprintf(output, "\tvcgt.f32 r%d, s%d, s%d\n", reg, left->place, right->place);
				} 
				else if ( node->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_LT ){
					free_freg(reg);
					reg = get_reg();
					if ( reg != -1 ){
						node->place = reg;
					}
					else{
						reg = 12;
					}

					fprintf(output, "\tldr r%d, =1\n", reg);
					fprintf(output, "\tvcmp.f32 s%d, s%d\n", left->place, right->place);
					fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
					fprintf(output, "\tblt " FLOAT_LABEL "%d\n", float_label_count);
					fprintf(output, "\tldr r%d, =0\n", reg);
					fprintf(output, FLOAT_LABEL"%d:\n", float_label_count);
					float_label_count++;
					//fprintf(output, "\tvclt.f32 r%d, s%d, s%d\n", reg, left->place, right->place);
				} 
			}
			free_freg(left->place);
			free_freg(right->place);
		}
		else if ( node->semantic_value.exprSemanticValue.kind == UNARY_OPERATION ){
			genExpr(left);
			memToReg(left, 'f');
			if ( node->semantic_value.exprSemanticValue.op.unaryOp == UNARY_OP_POSITIVE ){
				fprintf(output, "\tvmov.f32  s%d, s%d\n", reg, left->place);
			}
			else if ( node->semantic_value.exprSemanticValue.op.unaryOp == UNARY_OP_NEGATIVE ){
				fprintf(output, "\tvneg.f32 s%d, s%d\n", reg, left->place);

			}
			else if ( node->semantic_value.exprSemanticValue.op.unaryOp == UNARY_OP_LOGICAL_NEGATION ){
				free_freg(reg);
				reg = get_reg();
				if ( reg !=-1 ){
					node->place = reg;
				}
				else{
					reg = 12;
				}
				fprintf(output, "\tldr r%d, =1\n", reg);
				fprintf(output, "\tvcmp.f32 s%d, #0\n", left->place);
				fprintf(output, "\tVMRS APSR_nzcv, FPSCR\n");
				fprintf(output, "\tbeq " FLOAT_LABEL "%d\n", float_label_count);
				fprintf(output, "\tldr r%d =0\n", reg);
				fprintf(output, FLOAT_LABEL"%d:\n",  float_label_count);
				float_label_count++;
			}
			free_freg(left->place);
		}
		if ( reg == 24 ) {
			node->place = ARoffset;
			node->place_type = -1;
			regToMem(node, reg, 'f');
			ARoffset -= 4;
		}
		if ( reg == 12 ){
			node->place = ARoffset;
			node->place_type = -1;
			regToMem(node, reg, 'i');
			ARoffset -= 4;
		}
	}
	fprintf(output, "@ Out[visitExpr]\n");
}


void visitSymRef(AST_NODE *node){
	fprintf(output, "@ In[visitSymRef]\n");
	char* var_name = node->semantic_value.identifierSemanticValue.identifierName;
	SymbolTableEntry *entry = node->semantic_value.identifierSemanticValue.symbolTableEntry;
	int global = (entry->nestingLevel == 0);

	if ( node->semantic_value.identifierSemanticValue.kind == NORMAL_ID ){
		if ( node->dataType == INT_TYPE ){
			int reg = get_reg();
			if ( global ){
				if( reg != -1 ){
					node->place = reg;
					fprintf(output, "\tldr  r%d, =_%s\n", reg, var_name);
					fprintf(output, "\tldr  r%d, [r%d, #0]\n", reg, reg);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tldr  r12, =_%s\n", var_name);
					regToMem(node, 12, 'i');
				}
			}
			else{
				if( reg != -1 ){
					node->place = reg;
					fprintf(output, "\tldr  r%d, [fp, #%d]\n", reg, entry->offset);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tldr  r12, [fp, #%d]\n", entry->offset);
					regToMem(node, 12, 'i');
				}
			}
		}
		else if ( node->dataType == FLOAT_TYPE ){
			int freg = get_freg();
			if ( global ){
				if( freg != -1 ){
					int reg = get_reg();
					node->place = freg;
					fprintf(output, "\tldr  r%d, =_%s\n", reg, var_name);
					fprintf(output, "\tvldr.f32  s%d, [r%d]\n", freg, reg);
					//fprintf(output, "\tvldr.f32  s%d, [s%d, #0]\n", freg, freg);
					free_reg(reg);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tvldr.f32  s24, =_%s\n", var_name);
					regToMem(node, 24, 'f');
				}
			}
			else{
				if( freg != -1 ){
					node->place = freg;
					fprintf(output, "\tvldr.f32  s%d, [fp, #%d]\n", freg, entry->offset);
					//fprintf(output, "\tldr r%d, [fp, #%d]\n", reg, entry->offset);
					//fprintf(output, "\tvldr.f32  s%d, [r%d]\n", freg, reg);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tvldr.f32  s24, [fp, #%d]\n", entry->offset);
					regToMem(node, 24, 'f');
				}
			}
		}
	}
	else if ( node->semantic_value.identifierSemanticValue.kind == ARRAY_ID ){
		ArrayProperties property = entry->attribute->attr.typeDescriptor->properties.arrayProperties;

		int current_dimension = 0;
		AST_NODE* current_dimension_node = node->child;
		int total_offset_reg = get_reg();
		if (total_offset_reg != -1) {
		} 
		else {
			total_offset_reg = 12;
		}
		fprintf(output, "\tldr  r%d, =0\n", total_offset_reg);
		while(current_dimension_node != NULL){
			genExpr(current_dimension_node);
			if( current_dimension_node->place > 0 && current_dimension_node->place_type != -1 )
				fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, current_dimension_node->place);
			else{
				memToReg(current_dimension_node, 'i');
				fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, current_dimension_node->place);
			}
			free_reg(current_dimension_node->place);
			if ( current_dimension_node->rightSibling != NULL ) {
				int reg = get_reg();
				if (reg == -1){
					reg = 12;
				}
				fprintf(output, "\tldr r%d, =%d\n", reg, property.sizeInEachDimension[current_dimension+1]);
				fprintf(output, "\tmul r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, reg);
				free_reg(reg);
			} 
			else {
				int tmp = get_reg();
				fprintf(output, "\tmov r%d, #4\n", tmp);
				fprintf(output, "\tmul r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, tmp);
				free_reg(tmp);
			}
			current_dimension += 1;
			current_dimension_node = current_dimension_node->rightSibling;
		}
		if ( node->dataType == INT_TYPE ){
			int reg = get_reg();
			if ( global ){
				if( reg != -1 ){
					node->place = reg;
					fprintf(output, "\tldr r%d, =_%s\n", reg, var_name);
					fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, reg);
					//fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, entry->offset);
					
					fprintf(output, "\tldr r%d, [r%d, #%d]\n", reg, total_offset_reg, entry->offset);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tldr r12, =_%s\n", var_name);
					fprintf(output, "\tadd r%d, r%d, r12\n", total_offset_reg, total_offset_reg);
					//fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, entry->offset);
					fprintf(output, "\tldr r12, [r%d, #%d]\n", total_offset_reg, entry->offset);
					regToMem(node, 12, 'i');
				}
			}
			else{
				if( reg != -1 ){
					node->place = reg;
					fprintf(output, "\tadd r%d, r%d, fp\n",  total_offset_reg, total_offset_reg);
					fprintf(output, "\tadd r%d, r%d, #%d\n",  total_offset_reg, total_offset_reg, entry->offset);
					fprintf(output, "\tldr r%d, [r%d, #0]\n",  reg, total_offset_reg);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tadd r%d, r%d, fp\n",  total_offset_reg, total_offset_reg);
					fprintf(output, "\tadd r%d, r%d, #%d\n",  total_offset_reg, total_offset_reg, entry->offset);
					fprintf(output, "\tldr r%d, [r%d, #0]\n",  reg, total_offset_reg);
					regToMem(node, 12, 'i');
				}
			}
		}
		else if ( node->dataType == FLOAT_TYPE ){
			int freg = get_freg();	
			if ( global ){
				int tmp = get_reg();
				if ( tmp == -1 )
					tmp = 12;

				if( freg != -1 ){
					node->place = freg;
					fprintf(output, "\tldr r%d, =_%s\n", tmp, var_name);
					fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, tmp);
					//fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, entry->offset);

					fprintf(output, "\tvldr.f32 s%d, [r%d, #%d]\n", freg, total_offset_reg, entry->offset);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tldr r%d, =_%s\n", tmp, var_name);
					fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, tmp);
					//fprintf(output, "\tadd r%d, r%d, r%d\n", total_offset_reg, total_offset_reg, entry->offset);
					
					fprintf(output, "\tvldr.f32 s24, [r%d, #%d]\n", total_offset_reg, entry->offset);
					regToMem(node, 24, 'f');
				}
				free_reg(tmp);
			}
			else{
				if( freg != -1 ){
					node->place = freg;
					fprintf(output, "\tadd r%d, r%d, fp\n",  total_offset_reg, total_offset_reg);
					fprintf(output, "\tadd r%d, r%d, #%d\n",  total_offset_reg, total_offset_reg, entry->offset);
					fprintf(output, "\tvldr.f32 s%d, [r%d, #0]\n",  freg, total_offset_reg);
				}
				else{
					node->place = ARoffset;
					node->place_type = -1;
					ARoffset -= 4;
					fprintf(output, "\tadd r%d, r%d, fp\n",  total_offset_reg, total_offset_reg);
					fprintf(output, "\tadd r%d, r%d, #%d\n",  total_offset_reg, total_offset_reg, entry->offset);
					fprintf(output, "\tvldr.f32 s%d, [r%d, #0]\n",  freg, total_offset_reg);
					regToMem(node, 24, 'f');
				}
			}
		}
		free_reg(total_offset_reg);
	}

	fprintf(output, "@ Out[visitSymRef]\n");
}

void visitConst(AST_NODE *node){
	fprintf(output, "@ In[visitConst]\n");
	CON_Type *val = node->semantic_value.const1;

	if ( val->const_type == INTEGERC ){
		int reg = get_reg();
		if ( reg != -1 ){
			node->place = reg;
			fprintf(output, "\tldr r%d, =%d \n", reg, val->const_u.intval);
		}
		else{
			node->place =ARoffset;
			node->place_type = -1;
			ARoffset -= 4;
			fprintf(output, "\tldr r12, =%d \n",  val->const_u.intval);
			regToMem(node, 12, 'i');
		}
	}
	else if ( val->const_type == FLOATC ){
		int freg = get_freg();
		if ( freg != -1 ){
			node->place = freg;

			sprintf( float_pool[float_pool_count], FLOAT_POOL"%d: .float %f", float_pool_count, val->const_u.fval);
			//fprintf(output, FLOAT"%d: .float %f\n", float_pool_count, val->const_u.fval);
				
			int tmp = get_reg();
			//fprintf(output, "\tvldr.f32 s%d, =%f\n", freg, val->const_u.fval);

			fprintf(output, "\tldr r%d, =" FLOAT_POOL "%d\n", tmp, float_pool_count);
			
			fprintf(output, "\tvldr.f32 s%d, [r%d]\n", freg, tmp);
			float_pool_count++;
			free_reg(tmp);

			/*SymbolTableEntry *entry = node->semantic_value.identifierSemanticValue.symbolTableEntry;
			entry->offset = ARoffset;
			ARoffset -= 4;*/
		}
		else{
			node->place =ARoffset;
			node->place_type = -1;
			ARoffset -= 4;
			sprintf( float_pool[float_pool_count], FLOAT_POOL"%d: .float %f", float_pool_count, val->const_u.fval);
			int tmp = get_reg();
			fprintf(output, "\tldr r%d, =" FLOAT_POOL "%d\n", tmp, float_pool_count);
			fprintf(output, "\tvldr.f32 s24, [r%d]\n", tmp);
			float_pool_count++;
			regToMem(node, 24, 'f');
			free_reg(tmp);
		}
	}
	else if ( val->const_type == STRINGC ){
		node->place = string_label_count++;
		fprintf(output, ".data\n");

		val->const_u.sc[strlen(val->const_u.sc)-1] = '\0';
		strcat(val->const_u.sc, "\\000");
		strcat(val->const_u.sc, "\"");

		fprintf(output, STRING_LABEL "%d: .ascii %s\n", node->place, val->const_u.sc);
		fprintf(output, ".align 2\n");
		fprintf(output, ".text\n");
	}

	fprintf(output, "@ Out[visitConst]\n");
}


void genReadCall(AST_NODE *node){
	fprintf(output, "@ In[genReadCall]\n");
	node->place = ARoffset;
	node->place_type = -1;
	ARoffset -= 4;
	if ( node->dataType == INT_TYPE ){
		fprintf(output, "\tbl _read_int\n");
		int reg = get_reg();
		if ( reg != -1 ){
			fprintf(output, "\tmov r%d, r0\n", reg);
			fprintf(output, "\tstr r%d, [fp, #%d]\n", reg, node->place);
		}
		else{
			fprintf(output, "\tmov r12, r0\n");
			fprintf(output, "\tstr r12, [fp, #%d]\n", node->place);
		}
		free_reg(reg);
	}
	else if ( node->dataType == FLOAT_TYPE ){
		fprintf(output, "\tbl _read_float\n");
		int freg = get_freg();
		if ( freg != -1 ){
			fprintf(output, "\tvmov.f32 s%d, s0\n", freg);
			fprintf(output, "\tvstr.f32 s%d, [fp, #%d]\n", freg, node->place);
		}
		else{
			fprintf(output, "\tvmov.f32 s24, s0\n");
			fprintf(output, "\tvstr.f32 s24, [fp, #%d]\n", node->place);
		}
		free_freg(freg);
	}

	fprintf(output, "@ Out[genReadCall]\n");
}

void genWriteCall(AST_NODE *node){
	fprintf(output, "@ In[genWriteCall]\n");
	genExpr(node);
	if ( node->dataType == INT_TYPE ){
		//fprintf(output, "\tldr r4, [fp, #%d]\n", node->place); 
		//fprintf(output, "\tldr r%d, [fp, #-4]\n", node->place); 
		int reg;
		if ( node->place > 0 && node->place_type != -1 ){
			fprintf(output, "\tmov r0, r%d\n", node->place); 
			reg = node->place;
		}
		else{
			memToReg(node, 'i');
			fprintf(output, "\tmov r0, r12\n"); 
			reg = 12;
		}

		fprintf(output, "\tbl _write_int\n");
		free_reg(reg);
	}
	else if ( node->dataType == FLOAT_TYPE ){
		//fprintf(output, "\tvldr.f32 s24, [fp, #%d]\n", node->place); 
		//fprintf(output, "\tvmov.f32 s0, s24\n"); 
		int freg;
		if ( node->place > 0 && node->place_type != -1 ){
			fprintf(output, "\tvmov.f32 s0, s%d\n", node->place); 
			freg = node->place;
		}
		else{
			memToReg(node, 'f');
			fprintf(output, "\tvmov.f32 s0, s24\n"); 
			freg = 24;
		}

		fprintf(output, "\tbl _write_float\n");
		free_freg(freg);
	}
	else if ( node->dataType == CONST_STRING_TYPE ){
		int reg = get_reg();
		if ( reg == -1 ){
			reg = 12;
		}
		fprintf(output, "\tldr r%d, =" STRING_LABEL "%d\n", reg, node->place); 
		fprintf(output, "\tmov r0, r%d\n", reg); 
		fprintf(output, "\tbl _write_str\n");
		free_reg(reg);
	}
	fprintf(output, "@ Out[genWriteCall]\n");
}




void codeGeneration(AST_NODE *root){
	output = fopen("output.s", "w");
	if ( output == NULL ){
		return;
	}
	
	genProgramNode(root);
	print_float_pool();

	fclose(output);
	return;
}