#include "symbolTable.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
// This file is for reference only, you are not required to follow the implementation. //

int HASH(char * str) {
	int idx=0;
	while (*str){
		idx = idx << 1;
		idx+=*str;
		str++;
	}
	return (idx & (HASH_TABLE_SIZE-1));
}

SymbolTable symbolTable;

SymbolTableEntry* newSymbolTableEntry(int nestingLevel)
{
    SymbolTableEntry* symbolTableEntry = (SymbolTableEntry*)malloc(sizeof(SymbolTableEntry));
    symbolTableEntry->nextInHashChain = NULL;
    symbolTableEntry->prevInHashChain = NULL;
    symbolTableEntry->nextInSameLevel = NULL;
    symbolTableEntry->sameNameInOuterLevel = NULL;
    symbolTableEntry->attribute = NULL;
    symbolTableEntry->name = NULL;
    symbolTableEntry->nestingLevel = nestingLevel;
    return symbolTableEntry;
}

void removeFromHashTrain(int hashIndex, SymbolTableEntry* entry)
{
    SymbolTableEntry *previousEntry = symbolTable.hashTable[hashIndex];
    SymbolTableEntry *currentEntry = previousEntry->nextInHashChain;
    while( currentEntry != NULL ){
        if( currentEntry == entry ){ // find the entry
            // connect its previous entry to next entry
            previousEntry->nextInHashChain = currentEntry->nextInHashChain;
            if( currentEntry->nextInHashChain != NULL ){
            // connect its next entry to previous entry
                currentEntry->nextInHashChain->prevInHashChain = previousEntry;
            }
            free( currentEntry );
            return;
        }
        previousEntry = currentEntry;
        currentEntry = currentEntry->nextInHashChain;
    }
}

void enterIntoHashTrain(int hashIndex, SymbolTableEntry* entry)
{
    SymbolTableEntry *currentChain = symbolTable.hashTable[hashIndex];
    SymbolTableEntry *outerLevel = retrieveSymbol(entry->name);
    // link the entry with its outer level of the same name
    entry->sameNameInOuterLevel = outerLevel;

    SymbolTableEntry *scope = symbolTable.scopeDisplay[entry->nestingLevel];
    // search to the tail of the scope list
    while (scope->nextInSameLevel != NULL){
        scope = scope->nextInSameLevel;
    }
    // add the entry to the tail of the scope list
    scope->nextInSameLevel = entry;

    while (currentChain != NULL){
        if (currentChain->nextInHashChain == NULL){ // at the tail of the chain
            entry->prevInHashChain = currentChain; // set the previous entry
            entry->nextInHashChain = NULL; // no other entry behind the tail entry
            currentChain->nextInHashChain = entry; // add the entry in the tail
            break;
        }
        else if (currentChain->nextInHashChain->nestingLevel <= entry->nestingLevel){ // insert the entry in the middle of the chain
            entry->nextInHashChain = currentChain->nextInHashChain;
            currentChain->nextInHashChain->prevInHashChain = entry;
            currentChain->nextInHashChain = entry;
            entry->prevInHashChain = currentChain;
            break;
        }
        // find next entry in the chain
        currentChain = currentChain->nextInHashChain;
    }
}

void initializeSymbolTable()
{
    // initialize the member of SymbolTable
    symbolTable.currentLevel = 0;
    symbolTable.scopeDisplayElementCount = 256;
    symbolTable.scopeDisplay = (SymbolTableEntry **)malloc(sizeof(SymbolTableEntry *) * 256);
    symbolTable.scopeDisplay[0] = newSymbolTableEntry(0);

    // initialize the hash table
    int i;
    for(i = 0; i < HASH_TABLE_SIZE; ++i){
        symbolTable.hashTable[i] = newSymbolTableEntry(0);
    }

    // make many default entries to form the default values in the table
    // 1. SYMBOL_TABLE_INT_NAME
    SymbolAttribute *intAttr = (SymbolAttribute *)malloc( sizeof(SymbolAttribute) );
    intAttr->attributeKind = TYPE_ATTRIBUTE;
    intAttr->attr.typeDescriptor = (TypeDescriptor *)malloc( sizeof(TypeDescriptor) );
    intAttr->attr.typeDescriptor->kind = SCALAR_TYPE_DESCRIPTOR;
    intAttr->attr.typeDescriptor->properties.dataType = INT_TYPE;
    enterSymbol(SYMBOL_TABLE_INT_NAME, intAttr);

    // 2. SYMBOL_TABLE_FLOAT_NAME
    SymbolAttribute *floatAttr = (SymbolAttribute *)malloc( sizeof(SymbolAttribute) );
    floatAttr->attributeKind = TYPE_ATTRIBUTE;
    floatAttr->attr.typeDescriptor = (TypeDescriptor *)malloc( sizeof(TypeDescriptor) );
    floatAttr->attr.typeDescriptor->kind = SCALAR_TYPE_DESCRIPTOR;
    floatAttr->attr.typeDescriptor->properties.dataType = FLOAT_TYPE;
    enterSymbol(SYMBOL_TABLE_FLOAT_NAME, floatAttr);

    // 3. SYMBOL_TABLE_VOID_NAME
    SymbolAttribute *voidAttr = (SymbolAttribute *)malloc( sizeof(SymbolAttribute) );
    voidAttr->attributeKind = TYPE_ATTRIBUTE;
    voidAttr->attr.typeDescriptor = (TypeDescriptor *)malloc( sizeof(TypeDescriptor) );
    voidAttr->attr.typeDescriptor->kind = SCALAR_TYPE_DESCRIPTOR;
    voidAttr->attr.typeDescriptor->properties.dataType = VOID_TYPE;
    enterSymbol(SYMBOL_TABLE_VOID_NAME, voidAttr);

    // 4. SYMBOL_TABLE_SYS_LIB_READ
    SymbolAttribute *readAttr = (SymbolAttribute *)malloc( sizeof(SymbolAttribute) );
    readAttr->attributeKind = FUNCTION_SIGNATURE;
    readAttr->attr.functionSignature = (FunctionSignature *)malloc( sizeof(FunctionSignature) );
    readAttr->attr.functionSignature->parametersCount = 0;
    readAttr->attr.functionSignature->returnType = INT_TYPE;
    enterSymbol(SYMBOL_TABLE_SYS_LIB_READ, readAttr);

    // 5. SYMBOL_TABLE_SYS_LIB_FREAD
    SymbolAttribute *freadAttr = (SymbolAttribute *)malloc( sizeof(SymbolAttribute) );
    freadAttr->attributeKind = FUNCTION_SIGNATURE;
    freadAttr->attr.functionSignature = (FunctionSignature *)malloc( sizeof(FunctionSignature) );
    freadAttr->attr.functionSignature->parametersCount = 0;
    freadAttr->attr.functionSignature->returnType = FLOAT_TYPE;
    enterSymbol(SYMBOL_TABLE_SYS_LIB_FREAD, freadAttr);

    // 6. SYMBOL_TABLE_SYS_LIB_WRITE
    SymbolAttribute *writeAttr = (SymbolAttribute *)malloc( sizeof(SymbolAttribute) );
    writeAttr->attributeKind = FUNCTION_SIGNATURE;
    writeAttr->attr.functionSignature = (FunctionSignature *)malloc( sizeof(FunctionSignature) );
    writeAttr->attr.functionSignature->parametersCount = 1;
    // create the parameter for write function
    Parameter *writeParam = (Parameter *)malloc( sizeof(Parameter) );
    writeParam->type = (TypeDescriptor *)malloc( sizeof(TypeDescriptor) );
    writeParam->next = NULL;
    writeParam->type->kind = SCALAR_TYPE_DESCRIPTOR;
    writeParam->type->properties.dataType = VOID_TYPE;
    writeParam->parameterName = "oec";
    // add parameter to the write function
    writeAttr->attr.functionSignature->parameterList = writeParam;
    writeAttr->attr.functionSignature->returnType = VOID_TYPE;
    enterSymbol(SYMBOL_TABLE_SYS_LIB_WRITE, writeAttr);
}

void symbolTableEnd()
{
    int i;
    // free all the hash tables
    for( i = 0; i < HASH_TABLE_SIZE; ++i){
        free( symbolTable.hashTable[i] );
    }
}

SymbolTableEntry* retrieveSymbol(char* symbolName)
{
    int hashIdx = HASH( symbolName );
    SymbolTableEntry *currentEntry = symbolTable.hashTable[hashIdx]->nextInHashChain;
    while( currentEntry != NULL ){
        // find the entry in the hash table
        if( strcmp(currentEntry->name, symbolName)==0 ){
            return currentEntry;
        }
        // not found, search to next bucket
        currentEntry = currentEntry->nextInHashChain;
    }
    return NULL; // fail to find the symbolName in hash table
}

SymbolTableEntry* enterSymbol(char* symbolName, SymbolAttribute* attribute)
{
    SymbolTableEntry *newSymTable = newSymbolTableEntry(symbolTable.currentLevel);
    // assign attribute to the new symbol table
    newSymTable->attribute = attribute;
    // assign name to the new symbol table
    newSymTable->name = (char *)malloc( sizeof(char) * (strlen(symbolName)+1));
    strncpy(newSymTable->name, symbolName, strlen(symbolName));

    // insert the new entry into the hash chain
    enterIntoHashTrain( HASH(newSymTable->name), newSymTable );
    return newSymTable;
}

//remove the symbol from the current scope
void removeSymbol(char* symbolName)
{
    SymbolTableEntry* targetEntry = retrieveSymbol( symbolName );
    if( targetEntry == NULL ) return; // not found

    // previous entry connect to next entry
    if( targetEntry->prevInHashChain != NULL ){
        targetEntry->prevInHashChain->nextInHashChain = targetEntry->nextInHashChain;
    }
    // next entry connect to previous entry
    if( targetEntry->nextInHashChain != NULL ){
        targetEntry->nextInHashChain->prevInHashChain = targetEntry->prevInHashChain;
    }
}

int declaredLocally(char* symbolName)
{
    // search the level list in the local scope
    SymbolTableEntry *currentEntry = symbolTable.scopeDisplay[symbolTable.currentLevel]->nextInSameLevel;
    while( currentEntry != NULL ){
        if( strcmp( currentEntry->name, symbolName ) == 0 ){
            // the name has already declared in the local scope
            return 1;
        }
        currentEntry = currentEntry->nextInSameLevel;
    }
    return 0; // not declared in the local scope
}

void openScope()
{
    ++symbolTable.currentLevel; // in the next level of scope
    if( symbolTable.currentLevel >= symbolTable.scopeDisplayElementCount ){
        // double the size of scopeDisplay when scopeDisplay is inadequate
        symbolTable.scopeDisplayElementCount *= 2;
        symbolTable.scopeDisplay = (SymbolTableEntry **)realloc(symbolTable.scopeDisplay, sizeof(SymbolTableEntry *) * symbolTable.scopeDisplayElementCount);
    }
    symbolTable.scopeDisplay[symbolTable.currentLevel] = newSymbolTableEntry(0);
}

void closeScope()
{
    SymbolTableEntry *currentEntry = symbolTable.scopeDisplay[symbolTable.currentLevel]->nextInSameLevel, *previousEntry;

    // free all the entries in current scope
    while( currentEntry != NULL ){
        if( currentEntry->prevInHashChain != NULL ){
            currentEntry->prevInHashChain->nextInHashChain = currentEntry->nextInHashChain;
        }
        if( currentEntry->nextInHashChain != NULL ){
            currentEntry->nextInHashChain->prevInHashChain = currentEntry->prevInHashChain;
        }
        previousEntry = currentEntry;
        currentEntry = currentEntry->nextInSameLevel;
        free( previousEntry ); // release the entry in current scope
    }
    --symbolTable.currentLevel;
}
