#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"
#include "symbolTable.h"
// This file is for reference only, you are not required to follow the implementation. //
// You only need to check for errors stated in the hw4 document. //
int g_anyErrorOccur = 0;

DATA_TYPE getBiggerType(DATA_TYPE dataType1, DATA_TYPE dataType2);
void processProgramNode(AST_NODE *programNode);
void processDeclarationNode(AST_NODE* declarationNode);
void declareIdList(AST_NODE* typeNode, SymbolAttributeKind isVariableOrTypeAttribute, int ignoreArrayFirstDimSize);
void declareFunction(AST_NODE* returnTypeNode);
void processDeclDimList(AST_NODE* variableDeclDimList, TypeDescriptor* typeDescriptor, int ignoreFirstDimSize);
void processTypeNode(AST_NODE* typeNode);
void processBlockNode(AST_NODE* blockNode);
void processStmtNode(AST_NODE* stmtNode);
void processGeneralNode(AST_NODE *node);
void checkAssignOrExpr(AST_NODE* assignOrExprRelatedNode);
void checkWhileStmt(AST_NODE* whileNode);
void checkForStmt(AST_NODE* forNode);
void checkAssignmentStmt(AST_NODE* assignmentNode);
void checkIfStmt(AST_NODE* ifNode);
void checkWriteFunction(AST_NODE* functionCallNode);
void checkFunctionCall(AST_NODE* functionCallNode);
void processExprRelatedNode(AST_NODE* exprRelatedNode);
void checkParameterPassing(Parameter* formalParameter, AST_NODE* actualParameter);
void checkReturnStmt(AST_NODE* returnNode);
void processExprNode(AST_NODE* exprNode);
void processVariableLValue(AST_NODE* idNode);
void processVariableRValue(AST_NODE* idNode, int ignoreDim);
void processConstValueNode(AST_NODE* constValueNode);
void getExprOrConstValue(AST_NODE* exprOrConstNode, int* iValue, float* fValue);
void evaluateExprValue(AST_NODE* exprNode);


typedef enum ErrorMsgKind
{
    SYMBOL_IS_NOT_TYPE,
    SYMBOL_REDECLARE,
    SYMBOL_UNDECLARED,
    NOT_FUNCTION_NAME,
    TRY_TO_INIT_ARRAY,
    EXCESSIVE_ARRAY_DIM_DECLARATION,
    RETURN_ARRAY,
    VOID_VARIABLE,
    TYPEDEF_VOID_ARRAY,
    PARAMETER_TYPE_UNMATCH,
    TOO_FEW_ARGUMENTS,
    TOO_MANY_ARGUMENTS,
    RETURN_TYPE_UNMATCH,
    INCOMPATIBLE_ARRAY_DIMENSION,
    NOT_ASSIGNABLE,
    NOT_ARRAY,
    IS_TYPE_NOT_VARIABLE,
    IS_FUNCTION_NOT_VARIABLE,
    STRING_OPERATION,
    ARRAY_SIZE_NOT_INT,
    ARRAY_SIZE_NEGATIVE,
    ARRAY_SUBSCRIPT_NOT_INT,
    PASS_ARRAY_TO_SCALAR,
    PASS_SCALAR_TO_ARRAY
} ErrorMsgKind;

void printErrorMsgSpecial(AST_NODE* node1, char* name, ErrorMsgKind errorMsgKind)
{
    g_anyErrorOccur = 1;
    printf("Error found in line %d\n", node1->linenumber);
    
    switch (errorMsgKind) {
        case SYMBOL_UNDECLARED:
            printf("ID <%s> undeclared.\n", name);
            break;
        case SYMBOL_REDECLARE:
            printf("ID <%s> redeclared.\n", name);
            break;
        case TOO_MANY_ARGUMENTS:
            printf("too many arguments to function <%s>.\n", name);
            break;
        case TOO_FEW_ARGUMENTS:
            printf("too few arguments to function <%s>.\n", name);
            break;
        case PASS_ARRAY_TO_SCALAR:
            printf("Array <%s> passed to scalar parameter <%s>.\n", node1->semantic_value.identifierSemanticValue.identifierName, name);
            break;
        case PASS_SCALAR_TO_ARRAY:
            printf("Scalar <%s> passed to array parameter <%s>.\n", node1->semantic_value.identifierSemanticValue.identifierName, name);
            break;
        case SYMBOL_IS_NOT_TYPE:
            printf("Type <%s> is not a valid type", name);
            break;
        default:
            printf("Unhandled case in void printErrorMsgSpecial()\n");
            break;
    }
}


void printErrorMsg(AST_NODE* node, ErrorMsgKind errorMsgKind)
{
    g_anyErrorOccur = 1;
    printf("Error found in line %d\n", node->linenumber);
    
    switch(errorMsgKind)
    {
        case RETURN_TYPE_UNMATCH:
            puts("Incompatible return type.");
            break;
        case INCOMPATIBLE_ARRAY_DIMENSION:
            puts("Incompatible array dimensions.");
            break;
        case ARRAY_SUBSCRIPT_NOT_INT:
            printf("Array subscript is not an integer");
            break;
        default:
            printf("Unhandled case in void printErrorMsg(AST_NODE* node, ERROR_MSG_KIND* errorMsgKind)\n");
            break;
    }
}


void semanticAnalysis(AST_NODE *root)
{
    processProgramNode(root);
}


DATA_TYPE getBiggerType(DATA_TYPE dataType1, DATA_TYPE dataType2)
{
    if(dataType1 == FLOAT_TYPE || dataType2 == FLOAT_TYPE){
        return FLOAT_TYPE;
    }
    else{
        return INT_TYPE;
    }
}


void processProgramNode(AST_NODE *programNode)
{
    AST_NODE *child = programNode->child;
    while( child != NULL ){
        processDeclarationNode(child);
        child = child->rightSibling;
    }
    symbolTableEnd();
}

void processDeclarationNode(AST_NODE* declarationNode)
{
    AST_NODE *child = declarationNode->child;
    if( declarationNode->nodeType == VARIABLE_DECL_LIST_NODE ){ // variable declaration
        while( child != NULL ){
            processDeclarationNode(child);
            child = child->rightSibling;
        }
    }
    else if( declarationNode->semantic_value.declSemanticValue.kind == FUNCTION_DECL ){ // function declaration
        declareFunction( declarationNode );
    }
    else if( declarationNode->semantic_value.declSemanticValue.kind == VARIABLE_DECL || declarationNode->semantic_value.declSemanticValue.kind == TYPE_DECL ){
        declareIdList(declarationNode, declarationNode->semantic_value.declSemanticValue.kind, 0);
    }
    else if( declarationNode->semantic_value.declSemanticValue.kind == FUNCTION_PARAMETER_DECL ){
        declareIdList(declarationNode, VARIABLE_ATTRIBUTE, 1);
    }
}


void processTypeNode(AST_NODE* idNodeAsType)
{
    SymbolTableEntry *entry = retrieveSymbol( idNodeAsType->semantic_value.identifierSemanticValue.identifierName );

    if( entry == NULL ){ // id is undeclared
        printErrorMsgSpecial(idNodeAsType, idNodeAsType->semantic_value.identifierSemanticValue.identifierName, SYMBOL_UNDECLARED);
    }

    while( entry != NULL ){ // find the id with type declaration
        if (entry->attribute->attributeKind == TYPE_ATTRIBUTE)
            break;
        entry = entry->sameNameInOuterLevel;
    }
    // assign the entry to the AST node
    idNodeAsType->semantic_value.identifierSemanticValue.symbolTableEntry = entry;
}


void declareIdList(AST_NODE* declarationNode, SymbolAttributeKind isVariableOrTypeAttribute, int ignoreArrayFirstDimSize)
{
    AST_NODE *id = declarationNode->child->rightSibling;
    processTypeNode(declarationNode->child);
    if( declarationNode->child->semantic_value.identifierSemanticValue.symbolTableEntry == NULL ){ // id is not declared
        return;
    }

    SymbolTableEntry *currentEntry;
    SymbolAttribute *attribute;
    while( id != NULL ){
        currentEntry = retrieveSymbol(id->semantic_value.identifierSemanticValue.identifierName); // check whether the name of the variable or type is declared or not

        switch( isVariableOrTypeAttribute ){
            case TYPE_ATTRIBUTE: // declare a type
                while( currentEntry != NULL ){ // search
                    if( currentEntry->attribute->attributeKind == TYPE_ATTRIBUTE )
                        break;
                    currentEntry = currentEntry->sameNameInOuterLevel;
                }

                if( currentEntry != NULL ){ // id has already declared
                    printErrorMsgSpecial(id, id->semantic_value.identifierSemanticValue.identifierName, SYMBOL_REDECLARE);
                    break;
                }

                attribute = (SymbolAttribute *)malloc( sizeof(SymbolAttribute) );
                attribute->attr.typeDescriptor = declarationNode->child->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.typeDescriptor;
                attribute->attributeKind = isVariableOrTypeAttribute;
                id->semantic_value.identifierSemanticValue.symbolTableEntry = enterSymbol(id->semantic_value.identifierSemanticValue.identifierName, attribute);
                break;
            case VARIABLE_ATTRIBUTE: // declare a variable
                while( currentEntry != NULL ){ // search
                    if (currentEntry->attribute->attributeKind == VARIABLE_ATTRIBUTE)
                        break;
                    currentEntry = currentEntry->sameNameInOuterLevel;
                }

                if( currentEntry != NULL ){ // id has already declared
                    printErrorMsgSpecial(id, id->semantic_value.identifierSemanticValue.identifierName, SYMBOL_REDECLARE);
                    break;
                }
                attribute = (SymbolAttribute *)malloc( sizeof(SymbolAttribute) );
                attribute->attributeKind = isVariableOrTypeAttribute;
                if( id->semantic_value.identifierSemanticValue.kind == ARRAY_ID ){ // declare an array
                    TypeDescriptor *arrayType = (TypeDescriptor *)malloc( sizeof(TypeDescriptor) );
                    processDeclDimList(id, arrayType, ignoreArrayFirstDimSize);
                    if( arrayType->properties.arrayProperties.dimension == 0 ) break;

                    arrayType->properties.arrayProperties.elementType = declarationNode->child->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.typeDescriptor->properties.dataType;
                    attribute->attr.typeDescriptor = arrayType;
                }
                else{ // declare a normal variable
                    attribute->attr.typeDescriptor = declarationNode->child->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.typeDescriptor;
                }
                id->semantic_value.identifierSemanticValue.symbolTableEntry = enterSymbol(id->semantic_value.identifierSemanticValue.identifierName, attribute);
                if( id->semantic_value.identifierSemanticValue.kind == WITH_INIT_ID ){
                    if(id->child->nodeType == EXPR_NODE)
                        processExprNode(id->child);
                    else if(id->child->nodeType == STMT_NODE)
                        checkFunctionCall(id->child);
                    else if(id->child->nodeType == IDENTIFIER_NODE)
                        processVariableRValue(id->child, 0);
                    else
                        processConstValueNode(id->child);
                }
                break;
            default:
                printf("No define constant\n");
                break;
        }
        id = id->rightSibling; // find next
    }
}

void checkAssignOrExpr(AST_NODE* assignOrExprRelatedNode)
{
}

void checkWhileStmt(AST_NODE* whileNode)
{
    if(whileNode->child->nodeType == EXPR_NODE){
        processExprNode(whileNode->child);
    }
    else if(whileNode->child->nodeType == STMT_NODE){
        checkFunctionCall(whileNode->child);
    }
    else if(whileNode->child->nodeType == IDENTIFIER_NODE){
        processVariableRValue(whileNode->child, 0);
    }
    else{
        processConstValueNode(whileNode->child);
    }
    processBlockNode( whileNode->child->rightSibling );
}


void checkForStmt(AST_NODE* forNode)
{
    AST_NODE *child1 = forNode->child->child;
    AST_NODE *child2 = forNode->child->rightSibling->rightSibling->child;
    AST_NODE *expr    = forNode->child->rightSibling->child;
    AST_NODE *block   = forNode->child->rightSibling->rightSibling->rightSibling;

    while( child1 != NULL ){
        checkAssignmentStmt( child1 );
        child1 = child1->rightSibling;
    }
    while( expr != NULL ){
        processExprNode( expr );
        expr = expr->rightSibling;
    }
    while( child2 != NULL ){
        checkAssignmentStmt( child2 );
        child2 = child2->rightSibling;
    }
    processBlockNode( block );
}


void checkAssignmentStmt(AST_NODE* assignmentNode)
{
    AST_NODE *left = assignmentNode->child;
    AST_NODE *right = assignmentNode->child->rightSibling;
    processVariableRValue(left, 0);

    if(right->nodeType == EXPR_NODE){
        processExprNode( right );
    }
    else if(right->nodeType == STMT_NODE){
        checkFunctionCall( right );
    }
    else if(right->nodeType == IDENTIFIER_NODE){
        processVariableRValue(right, 0);
    }
    else{
        processConstValueNode( right );
    }
}


void checkIfStmt(AST_NODE* ifNode)
{
    processExprNode(ifNode->child);
    AST_NODE *ifBlock = ifNode->child->rightSibling;
    AST_NODE *elseBlock = ifNode->child->rightSibling->rightSibling;

    if( ifBlock->nodeType == STMT_NODE ){
        AST_NODE *tempNode = Allocate(STMT_NODE);
        tempNode->child = ifBlock;
        processStmtNode(tempNode);
        free(tempNode);
    }
    else if( ifBlock->nodeType == BLOCK_NODE ){
        processBlockNode(ifBlock);
    }

    if( elseBlock->nodeType == STMT_NODE ){
        if( elseBlock->semantic_value.stmtSemanticValue.kind == IF_STMT ){
            checkIfStmt(elseBlock);
        }
        else{
            AST_NODE *tempNode = Allocate(STMT_NODE);
            tempNode->child = elseBlock;
            processStmtNode(tempNode);
            free(tempNode);
        }

    }
    else if( elseBlock->nodeType == BLOCK_NODE ){
        processBlockNode(elseBlock);
    }
}

void checkWriteFunction(AST_NODE* functionCallNode)
{
    int paramCount, currentCount = 0;
    AST_NODE *currentParam = functionCallNode->child->rightSibling->child;
    SymbolTableEntry *entry = retrieveSymbol(functionCallNode->child->semantic_value.identifierSemanticValue.identifierName);

    while( entry != NULL ){ // search
        if( entry->attribute->attributeKind == FUNCTION_SIGNATURE ){
            break;
        }
        entry = entry->sameNameInOuterLevel;
    }

    if( entry == NULL ){ // id is undeclared
        printErrorMsgSpecial(functionCallNode->child, functionCallNode->child->semantic_value.identifierSemanticValue.identifierName, SYMBOL_UNDECLARED);
        return;
    }

    paramCount = entry->attribute->attr.functionSignature->parametersCount;
    while( currentParam != NULL ){ // count the parameter
        ++currentCount;
        currentParam = currentParam->rightSibling;
    }
    if(paramCount > currentCount){ // too few argument
        printErrorMsgSpecial(functionCallNode, functionCallNode->child->semantic_value.identifierSemanticValue.identifierName, TOO_FEW_ARGUMENTS);
    }
    else if( paramCount < currentCount ){ // too many arguments
        printErrorMsgSpecial(functionCallNode, functionCallNode->child->semantic_value.identifierSemanticValue.identifierName, TOO_MANY_ARGUMENTS);
    }

    functionCallNode->semantic_value.identifierSemanticValue.symbolTableEntry = entry;

}

void checkFunctionCall(AST_NODE* functionCallNode)
{
    checkWriteFunction( functionCallNode );
    SymbolTableEntry *targetParam = functionCallNode->semantic_value.identifierSemanticValue.symbolTableEntry;
    if( targetParam == NULL ) return;
    checkParameterPassing(targetParam->attribute->attr.functionSignature->parameterList, functionCallNode->child->rightSibling->child);
}

void checkParameterPassing(Parameter* formalParameter, AST_NODE* actualParameter)
{
    while( formalParameter != NULL && actualParameter != NULL ){
        if(actualParameter->nodeType == EXPR_NODE){
            processExprNode(actualParameter);
        }
        else if(actualParameter->nodeType == STMT_NODE){
            checkFunctionCall(actualParameter);
        }
        else if( actualParameter->nodeType == IDENTIFIER_NODE ){
            processVariableRValue(actualParameter, 1);
            if( actualParameter->semantic_value.identifierSemanticValue.symbolTableEntry == NULL ){
                continue;
            }

            TypeDescriptorKind formal = formalParameter->type->kind;
            IDENTIFIER_KIND actual = actualParameter->semantic_value.identifierSemanticValue.kind;
            if( formal == ARRAY_TYPE_DESCRIPTOR && actual == NORMAL_ID ){ // pass scalar to array
                printErrorMsgSpecial(actualParameter, formalParameter->parameterName, PASS_SCALAR_TO_ARRAY);
            }
            else if( formal == SCALAR_TYPE_DESCRIPTOR && actual == ARRAY_ID ){ // pass array to scalar
                printErrorMsgSpecial(actualParameter, formalParameter->parameterName, PASS_ARRAY_TO_SCALAR);
            }
        }
        else{
            processConstValueNode(actualParameter);
        }
        formalParameter = formalParameter->next;
        actualParameter = actualParameter->rightSibling;
    }
}


void processExprRelatedNode(AST_NODE* exprRelatedNode)
{
}

void getExprOrConstValue(AST_NODE* exprOrConstNode, int* iValue, float* fValue)
{
}

void evaluateExprValue(AST_NODE* exprNode)
{
}


void processExprNode(AST_NODE* exprNode)
{
    int lConst = 0, rConst = 0;
    AST_NODE *left = exprNode->child;
    AST_NODE *right = exprNode->child->rightSibling;
    DATA_TYPE lType, rType;
    exprNode->semantic_value.exprSemanticValue.isConstEval = 0;

    if( left->nodeType == EXPR_NODE ){
        processExprNode(left);
        lType = left->dataType;
        lConst = left->semantic_value.exprSemanticValue.isConstEval;
    }
    else if( left->nodeType == STMT_NODE ){
        checkFunctionCall(left);
        lType = left->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.functionSignature->returnType;
    }
    else if( left->nodeType == IDENTIFIER_NODE ){
        processVariableRValue(left, 0);
        lType = left->dataType;
    }
    else{
        lConst = 1;
        processConstValueNode(left);
        lType = left->dataType;
    }

    if( right != NULL ){
        if(right->nodeType == EXPR_NODE) {
            processExprNode( right );
            rType = right->dataType;
            rConst = right->semantic_value.exprSemanticValue.isConstEval;
        }
        else if(right->nodeType == STMT_NODE) {
            checkFunctionCall( right );
            rType = right->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.functionSignature->returnType;
        }
        else if(right->nodeType == IDENTIFIER_NODE) {
            processVariableRValue(right, 0);
            rType = right->dataType;
        }
        else {
            rConst = 1;
            processConstValueNode( right );
            rType = right->dataType;
        }
    }

    if( exprNode->semantic_value.exprSemanticValue.kind == UNARY_OPERATION ){
        exprNode->dataType = lType;
        if(lConst == 1) {
            exprNode->semantic_value.exprSemanticValue.isConstEval = 1;
        }
    }
    else if( exprNode->semantic_value.exprSemanticValue.op.binaryOp >= BINARY_OP_EQ && exprNode->semantic_value.exprSemanticValue.op.binaryOp <= BINARY_OP_LT ){
        exprNode->dataType = INT_TYPE;
        if(lConst == 1 && rConst == 1) {
            exprNode->semantic_value.exprSemanticValue.isConstEval = 1;
        }
    }
    else{
        exprNode->dataType = getBiggerType(lType, rType);
        if( lConst == 1 && rConst == 1 ){
            exprNode->semantic_value.exprSemanticValue.isConstEval = 1;
        }
    }
}


void processVariableLValue(AST_NODE* idNode)
{
}

void processVariableRValue(AST_NODE* idNode, int ignoreDim)
{
    SymbolTableEntry *entry = retrieveSymbol(idNode->semantic_value.identifierSemanticValue.identifierName);
    int dimensionCount;
    AST_NODE *dim;
    DATA_TYPE dimType;
    if( entry == NULL ){ // id is undeclared
        printErrorMsgSpecial(idNode, idNode->semantic_value.identifierSemanticValue.identifierName, SYMBOL_UNDECLARED);
        idNode->semantic_value.identifierSemanticValue.symbolTableEntry = entry;
        return;
    }
    idNode->semantic_value.identifierSemanticValue.symbolTableEntry = entry;
    if( entry->attribute->attr.typeDescriptor->kind == ARRAY_TYPE_DESCRIPTOR ){
        dimensionCount = 0;
        dim = idNode->child;
        while( dim != NULL ){
            ++dimensionCount;
            if( dim->nodeType == EXPR_NODE ){
                processExprNode( dim );
                dimType = dim->dataType;
            }
            else if( dim->nodeType == STMT_NODE ){
                checkFunctionCall(dim);
                dimType = dim->child->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.functionSignature->returnType;
            }
            else if( dim->nodeType == IDENTIFIER_NODE ){
                processVariableRValue(dim, 0);
                dimType = dim->dataType;
            }
            else{
                processConstValueNode( dim );
                dimType = dim->dataType;
            }

            if( dimType != INT_TYPE ){ // the subscript of array is not an integer
                printErrorMsg(idNode, ARRAY_SUBSCRIPT_NOT_INT);
            }
        }

        if( ignoreDim ){
            if(dimensionCount == entry->attribute->attr.typeDescriptor->properties.arrayProperties.dimension){
                idNode->semantic_value.identifierSemanticValue.kind = NORMAL_ID;
            }
            else if(dimensionCount > entry->attribute->attr.typeDescriptor->properties.arrayProperties.dimension){
                printErrorMsg(idNode, INCOMPATIBLE_ARRAY_DIMENSION);
            }
            else{
                idNode->semantic_value.identifierSemanticValue.kind = ARRAY_ID;
            }
        }
        else if( dimensionCount != entry->attribute->attr.typeDescriptor->properties.arrayProperties.dimension ){
            printErrorMsg(idNode, INCOMPATIBLE_ARRAY_DIMENSION);
        }
        idNode->dataType = entry->attribute->attr.typeDescriptor->properties.arrayProperties.elementType;
    }
    else{
        idNode->dataType = entry->attribute->attr.typeDescriptor->properties.dataType;
    }
}


void processConstValueNode(AST_NODE* constValueNode)
{
    if( constValueNode->semantic_value.const1->const_type == INTEGERC ){
        constValueNode->dataType = INT_TYPE;
    }
    else{
        constValueNode->dataType = FLOAT_TYPE;
    }
}


void checkReturnStmt(AST_NODE* returnNode)
{
    AST_NODE *parentNode = returnNode->parent;
    AST_NODE *expr = returnNode->child;
    AST_NODE *typeNode;
    while( parentNode != NULL ){
        if( parentNode->nodeType == DECLARATION_NODE ){
            if( parentNode->semantic_value.declSemanticValue.kind == FUNCTION_DECL ){
                break;
            }
        }
        parentNode = parentNode->parent;
    }
    typeNode = parentNode->child;
    if(typeNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.typeDescriptor->properties.dataType == VOID_TYPE) {
        if(expr != NULL) {
            printErrorMsg(returnNode, RETURN_TYPE_UNMATCH);
        }
        return;
    }
    else if( expr->nodeType == EXPR_NODE ){
        processExprNode(expr);
    }
    else if( expr->nodeType == STMT_NODE ){
        checkFunctionCall(expr);
    }
    else if( expr->nodeType == IDENTIFIER_NODE ){
        processVariableRValue(expr, 0);
    }
    else{
        processConstValueNode(expr);
    }

    if(typeNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.typeDescriptor->properties.dataType != expr->dataType){
        printErrorMsg(returnNode, RETURN_TYPE_UNMATCH);
    }
}


void processBlockNode(AST_NODE* blockNode)
{
    AST_NODE *child = blockNode->child;
    openScope();
    while( child ){
        switch( child->nodeType ){
            case STMT_LIST_NODE:
                processStmtNode(child);
                break;
            case VARIABLE_DECL_LIST_NODE:
                processDeclarationNode(child);
                break;
            default:
                break;
        }
        child = child->rightSibling;
    }
    closeScope();
}


void processStmtNode(AST_NODE* stmtNode)
{
    AST_NODE *child = stmtNode->child;
    while( child ){
        if( child->nodeType == BLOCK_NODE ){
            processBlockNode(child);
        }
        else{
            switch( child->semantic_value.stmtSemanticValue.kind ){
                case ASSIGN_STMT:
                    checkAssignmentStmt(child);
                    break;
                case FOR_STMT:
                    checkForStmt(child);
                    break;
                case WHILE_STMT:
                    checkWhileStmt(child);
                    break;
                case IF_STMT:
                    checkIfStmt(child);
                    break;
                case FUNCTION_CALL_STMT:
                    checkFunctionCall(child);
                    break;
                case RETURN_STMT:
                    checkReturnStmt(child);
                    break;
                default:
                    break;
            }
        }
        child = child->rightSibling;
    }
}

void processGeneralNode(AST_NODE *node)
{
}

void processDeclDimList(AST_NODE* idNode, TypeDescriptor* typeDescriptor, int ignoreFirstDimSize)
{
    int count = 0; // count the dimesion of the array
    AST_NODE *dim = idNode->child;
    DATA_TYPE dimType;

    while( dim != NULL ){
        if( count == 0 && ignoreFirstDimSize ){
            typeDescriptor->properties.arrayProperties.sizeInEachDimension[count] = 0;
        }
        else{
            if( dim->nodeType == EXPR_NODE ){
                processExprNode(dim);
                dimType = dim->dataType;
            }
            else if( dim->nodeType == IDENTIFIER_NODE ){
                processVariableRValue(dim, 0);
                dimType = dim->dataType;
            }
            else if( dim->nodeType == STMT_NODE ){
                checkFunctionCall(dim);
                dimType = dim->child->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.functionSignature->returnType;
            }
            else {
                processConstValueNode(dim);
                dimType = dim->dataType;
            }

            if( dimType != INT_TYPE ){ // subscription is not an integer
                printErrorMsg(idNode, ARRAY_SUBSCRIPT_NOT_INT);
                typeDescriptor->properties.arrayProperties.dimension = 0;
                return;
            }
            typeDescriptor->properties.arrayProperties.sizeInEachDimension[count] = 256;
        }
        ++count;
        dim = dim->rightSibling;
    }

    typeDescriptor->kind = ARRAY_TYPE_DESCRIPTOR;
    typeDescriptor->properties.arrayProperties.dimension = count;
}

void declareFunction(AST_NODE* declarationNode)
{
    processTypeNode( declarationNode->child );
    int count = 0;
    AST_NODE *id = declarationNode->child->rightSibling;
    AST_NODE *param = declarationNode->child->rightSibling->rightSibling->child;
    AST_NODE *block = declarationNode->child->rightSibling->rightSibling->rightSibling;

    if( declarationNode->child->semantic_value.identifierSemanticValue.symbolTableEntry == NULL ){
        return;
    }
    SymbolTableEntry *entry = retrieveSymbol(id->semantic_value.identifierSemanticValue.identifierName);

    while( entry != NULL ){ // search
        if( entry->attribute->attributeKind == FUNCTION_SIGNATURE ){
            break;
        }
        entry = entry->sameNameInOuterLevel;
    }
    if( entry != NULL ){ // not found
        printErrorMsgSpecial(id, id->semantic_value.identifierSemanticValue.identifierName, SYMBOL_REDECLARE);
        return;
    }

    SymbolAttribute *attribute = (SymbolAttribute *)malloc( sizeof(SymbolAttribute) );
    attribute->attributeKind = FUNCTION_SIGNATURE;
    openScope();
    Parameter *currentParam = NULL, *headParam = NULL, *previousParam = NULL;
    while( param != NULL ){
        ++count;
        processDeclarationNode( param );
        currentParam = (Parameter *)malloc( sizeof(Parameter) );
        if( headParam == NULL ){
            headParam = currentParam;
        }
        else{
            previousParam->next = currentParam;
        }
        currentParam->parameterName = param->child->rightSibling->semantic_value.identifierSemanticValue.identifierName;
        currentParam->next = NULL;
        currentParam->type = param->child->rightSibling->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.typeDescriptor;
        param = param->rightSibling;
        previousParam = currentParam;
        currentParam = currentParam->next;
    }
    attribute->attr.functionSignature = (FunctionSignature *)malloc( sizeof(FunctionSignature));
    attribute->attr.functionSignature->parameterList = headParam;
    attribute->attr.functionSignature->parametersCount = count;
    attribute->attr.functionSignature->returnType = declarationNode->child->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.typeDescriptor->properties.dataType;
    id->semantic_value.identifierSemanticValue.symbolTableEntry = enterSymbol(id->semantic_value.identifierSemanticValue.identifierName, attribute);
    processBlockNode(block);
    closeScope();
    id->semantic_value.identifierSemanticValue.symbolTableEntry = enterSymbol(id->semantic_value.identifierSemanticValue.identifierName, attribute);
}
